// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'package:doasdk/controller/main_controller.dart';
import 'package:doasdk/model/complete_registration_model.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'doasdk.dart';
import 'package:doasdk/pages/unknown_route.dart';

class DoaSDK {
  /// site key dari google enterprise https://cloud.google.com/recaptcha-enterprise,
  ///
  /// untuk mendapatkan site key kalian perlu signup atau generate jika sudah teregristrasi
  void setRecaptchaSiteKey(String? siteKey) {
    assert(siteKey != null);
    recaptchaSiteKey = siteKey!;
  }

  /// initServer == IP:PORT/initialiaze
  ///
  /// initServer == IP:PORT/checkResult
  void setZolozServer({
    @required String? initServer,
    @required String? checkServer,
  }) {
    zolozInitServer = initServer!;
    zolozCheckServer = checkServer!;
  }

  /// version 1.0.6
  Future<void> runSdk(
    BuildContext? context, {
    String? route,
    @required void Function(int, String)? onInterrupt,
    @required void Function(int, CompleteRegistrationModel?)? onComplete,
  }) async {
    await initStorage();
    await clearUserData();
    await initializeDateFormatting().then((value) {
      Intl.defaultLocale = "in";
    });
    assert(context != null);
    clientContext = context;
    onInterruptCallback = onInterrupt;
    onCompleteCallback = onComplete;
    Navigator.push(
      context!,
      MaterialPageRoute(
          builder: (context) => WillPopScope(
              onWillPop: _onBack(context),
              child: App(
                route: route,
              ))),
    );
  }

  Future<bool> Function()? _onBack(BuildContext context) {
    return () async {
      try {
        if (Get.currentRoute == "/") {
          Get.close(1);
          onInterruptCallback!(0, "interrupted");
          return true;
        } else if (Get.currentRoute == "/completeRegistration") {
          onCompleteCallback!(200, completeRegistrationModel);
          return true;
        } else {
          Get.close(1);
          return false;
        }
      } catch (e) {
        Get.close(1);
        return false;
      }
    };
  }
}

class App extends StatelessWidget {
  final String? route;
  const App({Key? key, this.route}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        routingCallback: (r) async {
          final MainController _mController = Get.find();
          final _pretakeFileRoute = [
            ROUTE.takeKtp.name,
            ROUTE.takeSignature.name,
            ROUTE.takeNpwp.name,
          ];
          if (r?.current == ROUTE.inputPhoneNumber.name) {
            await _mController.checkPermission(Permission.location);
          } else if (_pretakeFileRoute.contains(r?.current)) {
            try {
              await _mController.checkPermission(Permission.camera,
                  optionalPermissionName: " & Audio");
              await _mController.initCameraController(
                  front: ROUTE.selfieAndKtp.name == r?.current);
            } catch (e) {
              return;
            }
          } else if (r?.current == ROUTE.dataVerification.name) {
            // await _mController.notificationPermission();
          }
        },
        onInit: () {
          Get.put(MainController());
        },
        defaultTransition: Transition.size,
        transitionDuration: const Duration(milliseconds: 500),
        debugShowCheckedModeBanner: false,
        theme: ThemeData(fontFamily: 'montserrat'),
        initialRoute: route ?? ROUTE.openingAccount.name,
        getPages: routePage,
        unknownRoute: GetPage(
          name: '/notfound',
          page: () => const UnknownRoute(),
        ));
  }
}
