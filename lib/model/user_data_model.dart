// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

class UserDataModel {
  String? number;
  String? country;
  String? cardType;
  String? nik;
  String? fullName;
  String? dob;
  String? referralCode;
  String? pob;
  String? gender;
  String? address;
  String? rt;
  String? rw;
  String? province;
  String? subdistrict;
  String? regency;
  String? village;
  String? religion;
  String? postalCode;
  String? issuanceCity;
  String? email;
  String? phone;
  String? phoneHouse;
  String? npwp;
  String? maritalStatus;
  String? motherName;
  String? job;
  String? monthlyIncome;
  String? sourceFunds;
  String? estimatedTransaction;
  String? purposeAccount;
  String? jobPosition;
  String? workPlaceName;
  String? workPlacePhone;
  String? workPlaceAddress;
  String? officeProvince;
  String? officeSubdistrict;
  String? officeBranch;
  String? officeBranchAddress;

  UserDataModel({
    this.number,
    this.country,
    this.cardType,
    this.nik,
    this.fullName,
    this.dob,
    this.referralCode,
    this.pob,
    this.gender,
    this.address,
    this.rt,
    this.rw,
    this.province,
    this.subdistrict,
    this.regency,
    this.village,
    this.religion,
    this.postalCode,
    this.issuanceCity,
    this.email,
    this.phone,
    this.phoneHouse,
    this.npwp,
    this.maritalStatus,
    this.motherName,
    this.job,
    this.monthlyIncome,
    this.sourceFunds,
    this.estimatedTransaction,
    this.purposeAccount,
    this.jobPosition,
    this.workPlaceName,
    this.workPlacePhone,
    this.workPlaceAddress,
    this.officeProvince,
    this.officeSubdistrict,
    this.officeBranch,
    this.officeBranchAddress,
  });
  UserDataModel.fromJson(Map<String, dynamic> json) {
    number = json['number']?.toString();
    country = json['country']?.toString();
    cardType = json['card_type']?.toString();
    nik = json['nik']?.toString();
    fullName = json['full_name']?.toString();
    dob = json['dob']?.toString();
    referralCode = json['referral_code']?.toString();
    pob = json['pob']?.toString();
    gender = json['gender']?.toString();
    address = json['address']?.toString();
    rt = json['rt']?.toString();
    rw = json['rw']?.toString();
    province = json['province']?.toString();
    subdistrict = json['subdistrict']?.toString();
    regency = json['regency']?.toString();
    village = json['village']?.toString();
    religion = json['religion']?.toString();
    postalCode = json['postal_code']?.toString();
    issuanceCity = json['issuance_city']?.toString();
    email = json['email']?.toString();
    phone = json['phone']?.toString();
    phoneHouse = json['phone_house']?.toString();
    npwp = json['npwp']?.toString();
    maritalStatus = json['marital_status']?.toString();
    motherName = json['mother_name']?.toString();
    job = json['job']?.toString();
    monthlyIncome = json['monthly_income']?.toString();
    sourceFunds = json['source_funds']?.toString();
    estimatedTransaction = json['estimated_transaction']?.toString();
    purposeAccount = json['purpose_account']?.toString();
    jobPosition = json['job_position']?.toString();
    workPlaceName = json['work_place_name']?.toString();
    workPlacePhone = json['work_place_phone']?.toString();
    workPlaceAddress = json['work_place_address']?.toString();
    officeProvince = json['office_province']?.toString();
    officeSubdistrict = json['office_subdistrict']?.toString();
    officeBranch = json['office_branch']?.toString();
    officeBranchAddress = json['office_branch_address']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['number'] = number;
    data['country'] = country;
    data['card_type'] = cardType;
    data['nik'] = nik;
    data['full_name'] = fullName;
    data['dob'] = dob;
    data['referral_code'] = referralCode;
    data['pob'] = pob;
    data['gender'] = gender;
    data['address'] = address;
    data['rt'] = rt;
    data['rw'] = rw;
    data['province'] = province;
    data['subdistrict'] = subdistrict;
    data['regency'] = regency;
    data['village'] = village;
    data['religion'] = religion;
    data['postal_code'] = postalCode;
    data['issuance_city'] = issuanceCity;
    data['email'] = email;
    data['phone'] = phone;
    data['phone_house'] = phoneHouse;
    data['npwp'] = npwp;
    data['marital_status'] = maritalStatus;
    data['mother_name'] = motherName;
    data['job'] = job;
    data['monthly_income'] = monthlyIncome;
    data['source_funds'] = sourceFunds;
    data['estimated_transaction'] = estimatedTransaction;
    data['purpose_account'] = purposeAccount;
    data['job_position'] = jobPosition;
    data['work_place_name'] = workPlaceName;
    data['work_place_phone'] = workPlacePhone;
    data['work_place_address'] = workPlaceAddress;
    data['office_province'] = officeProvince;
    data['office_subdistrict'] = officeSubdistrict;
    data['office_branch'] = officeBranch;
    data['office_branch_address'] = officeBranchAddress;
    return data;
  }
}
