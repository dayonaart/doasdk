// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

class CompleteRegistrationModel {
  String? accountNumber;
  String? virtualNumber;
  String? customersName;
  String? cardType;
  String? createdDate;
  String? createdTime;
  String? officeBranch;
  String? cardDetail;

  CompleteRegistrationModel({
    this.accountNumber,
    this.virtualNumber,
    this.customersName,
    this.cardType,
    this.createdDate,
    this.createdTime,
    this.officeBranch,
    this.cardDetail,
  });
  CompleteRegistrationModel.fromJson(Map<String, dynamic> json) {
    accountNumber = json['account_number']?.toString();
    virtualNumber = json['virtual_number']?.toString();
    customersName = json['customers_name']?.toString();
    cardType = json['card_type']?.toString();
    createdDate = json['created_date']?.toString();
    createdTime = json['created_time']?.toString();
    officeBranch = json['office_branch']?.toString();
    cardDetail = json['card_detail']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['account_number'] = accountNumber;
    data['virtual_number'] = virtualNumber;
    data['customers_name'] = customersName;
    data['card_type'] = cardType;
    data['created_date'] = createdDate;
    data['created_time'] = createdTime;
    data['office_branch'] = officeBranch;
    data['card_detail'] = cardDetail;
    return data;
  }
}
