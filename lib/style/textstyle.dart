// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:doasdk/doasdk.dart';

TextStyle textStyleW500({double? fontSize, Color? fontColor}) {
  return TextStyle(
      fontWeight: FontWeight.w500, fontSize: fontSize, color: fontColor);
}

TextStyle textStyleW600({
  double? fontSize,
  Color? fontColor,
  TextDecoration? textDecoration,
}) {
  return TextStyle(
    fontWeight: FontWeight.w600,
    fontSize: fontSize,
    color: fontColor,
    decoration: textDecoration,
  );
}

TextStyle textStyleW700({double? fontSize, Color? fontColor}) {
  return TextStyle(
      fontWeight: FontWeight.w700, fontSize: fontSize, color: fontColor);
}
