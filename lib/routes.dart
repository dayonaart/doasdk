// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:doasdk/pages/complete_registration.dart';
import 'package:doasdk/pages/create_pin_account.dart';
import 'package:doasdk/pages/input_otp_code.dart';
import 'package:doasdk/pages/account_type.dart';
import 'package:doasdk/pages/data_verification.dart';
import 'package:doasdk/pages/liveness_verification.dart';
import 'package:doasdk/pages/photo_validation.dart';
import 'package:doasdk/pages/input_phone_number.dart';
import 'package:doasdk/pages/pretake_ktp.dart';
import 'package:doasdk/pages/opening_account.dart';
import 'package:doasdk/pages/opening_tnc.dart';
import 'package:doasdk/pages/pretake_npwp.dart';
import 'package:doasdk/pages/preview_take_image_camera.dart';
import 'package:doasdk/pages/processing_data.dart';
import 'package:doasdk/pages/registration_form.dart';
import 'package:doasdk/pages/registration_form_job_detail.dart';
import 'package:doasdk/pages/registration_form_office_branch.dart';
import 'package:doasdk/pages/registration_form_private.dart';
import 'package:doasdk/pages/selfie_and_ktp.dart';
import 'package:doasdk/pages/take_ktp.dart';
import 'package:doasdk/pages/pretake_signature.dart';
import 'package:doasdk/pages/take_npwp.dart';
import 'package:doasdk/pages/take_signature.dart';
import 'doasdk.dart';

///1. onBoarding,
///
///2. preRegister,
///
///3. openingAccount,
///
///4. openingTnc,
///
///5. inputPhoneNumber,
///
///6. accountType,
///
///7. preTakeKtp,
///
///8. takeKtp,
///
///9. previewTakeImage { args0:appbar title(string), args1:progress data(int) },
///
///10. registrationForm,
///
///11. registrationFormPrivate,
///
///12. registrationFormJobDetail,
///
///13. registrationFormOfficeBranch,
///
///14. livenessVerification,
///
///15. selfieAndKtp,
///
///17. preTakeSignature,
///
///18. takeSignature,
///
///19. preTakeNpwp,
///
///20. takeNpwp,
///
///21. photoValidation,
///
///22. dataVerification,
///
///23. inputOtpCode,
///
///24. createPinAccount,
///
///25. processingData,
///
///26. completeRegistration,
///
enum ROUTE {
  // onBoarding,
  // preRegister,
  openingAccount,
  previewTakeImage,
  openingTnc,
  inputPhoneNumber,
  accountType,
  preTakeKtp,
  takeKtp,
  registrationForm,
  registrationFormPrivate,
  registrationFormJobDetail,
  registrationFormOfficeBranch,
  livenessVerification,
  selfieAndKtp,

  ///args0: [appbar title]
  ///args1: [progress data]
  preTakeSignature,
  takeSignature,
  preTakeNpwp,
  takeNpwp,
  photoValidation,
  dataVerification,
  inputOtpCode,
  createPinAccount,
  processingData,
  completeRegistration
}

extension Page on ROUTE {
  String get name {
    switch (this) {
      // case ROUTE.onBoarding:
      //   return '/';

      // case ROUTE.preRegister:
      //   return '/preRegister';
      case ROUTE.openingAccount:
        return '/';
      case ROUTE.previewTakeImage:
        return '/previewImageTakeCamera';
      case ROUTE.openingTnc:
        return '/openingTnc';
      case ROUTE.inputPhoneNumber:
        return '/inputPhoneNumber';
      case ROUTE.accountType:
        return '/accountType';
      case ROUTE.preTakeKtp:
        return '/preTakeKtp';
      case ROUTE.takeKtp:
        return '/takeKtp';
      case ROUTE.registrationForm:
        return '/registrationForm';
      case ROUTE.registrationFormPrivate:
        return '/registrationFormPrivate';
      case ROUTE.registrationFormJobDetail:
        return '/registrationFormJobDetail';
      case ROUTE.registrationFormOfficeBranch:
        return '/registrationFormOfficeBranch';
      case ROUTE.livenessVerification:
        return '/livenessVerification';
      case ROUTE.selfieAndKtp:
        return '/selfieAndKtp';
      case ROUTE.preTakeSignature:
        return '/pretakeSignature';
      case ROUTE.takeSignature:
        return '/takeSignature';
      case ROUTE.preTakeNpwp:
        return '/preTakeNpwp';
      case ROUTE.takeNpwp:
        return '/takeNpwp';
      case ROUTE.photoValidation:
        return "/photoValidation";
      case ROUTE.dataVerification:
        return "/dataVerification";
      case ROUTE.inputOtpCode:
        return "/inputOtpCode";
      case ROUTE.createPinAccount:
        return "/createPinAccount";
      case ROUTE.processingData:
        return "/processingData";
      case ROUTE.completeRegistration:
        return "/completeRegistration";
      default:
        return "";
    }
  }
}

List<GetPage> get routePage {
  return [
    // GetPage(
    //     name: ROUTE.onBoarding.name,
    //     page: () {
    //       return OnBoarding();
    //     }),
    GetPage(
        name: ROUTE.previewTakeImage.name,
        page: () {
          return PreviewTakeImage();
        }),
    // GetPage(
    //     name: ROUTE.preRegister.name,
    //     page: () {
    //       return const PreRegister();
    //     }),
    GetPage(
        name: ROUTE.openingAccount.name,
        page: () {
          return const OpeningAccount();
        }),
    GetPage(
        name: ROUTE.openingTnc.name,
        page: () {
          return OpeningTnc();
        }),
    GetPage(
        name: ROUTE.inputPhoneNumber.name,
        page: () {
          return InputPhoneNumber();
        }),
    GetPage(
        name: ROUTE.accountType.name,
        page: () {
          return AccountType();
        }),
    GetPage(
        name: ROUTE.preTakeKtp.name,
        page: () {
          return PreTakeKtp();
        }),
    GetPage(
        name: ROUTE.takeKtp.name,
        page: () {
          return TakeKtp();
        }),
    GetPage(
        name: ROUTE.registrationForm.name,
        page: () {
          return RegistrationForm();
        }),
    GetPage(
        name: ROUTE.registrationFormPrivate.name,
        page: () {
          return RegistrationFormPrivate();
        }),
    GetPage(
        name: ROUTE.registrationFormJobDetail.name,
        page: () {
          return RegistrationFormJobDetail();
        }),
    GetPage(
        name: ROUTE.registrationFormOfficeBranch.name,
        page: () {
          return RegistrationFormOfficeBranch();
        }),
    GetPage(
        name: ROUTE.livenessVerification.name,
        page: () {
          return LivenessVerification();
        }),
    GetPage(
        name: ROUTE.selfieAndKtp.name,
        page: () {
          return SelfieAndKtp();
        }),
    GetPage(
        name: ROUTE.preTakeSignature.name,
        page: () {
          return PreTakeSignature();
        }),
    GetPage(
        name: ROUTE.takeSignature.name,
        page: () {
          return TakeSignature();
        }),
    GetPage(
        name: ROUTE.preTakeNpwp.name,
        page: () {
          return PreTakeNpwp();
        }),
    GetPage(
        name: ROUTE.takeNpwp.name,
        page: () {
          return TakeNpwp();
        }),
    GetPage(
        name: ROUTE.photoValidation.name,
        page: () {
          return PhotoValidation();
        }),
    GetPage(
        name: ROUTE.dataVerification.name,
        page: () {
          return DataVerification();
        }),
    GetPage(
        name: ROUTE.inputOtpCode.name,
        page: () {
          return InputOtpCode();
        }),
    GetPage(
        name: ROUTE.createPinAccount.name,
        page: () {
          return CreatePinAccount();
        }),
    GetPage(
        name: ROUTE.processingData.name,
        page: () {
          return ProcessingData();
        }),
    GetPage(
        name: ROUTE.completeRegistration.name,
        page: () {
          return CompleteRegistration();
        }),
  ];
}
