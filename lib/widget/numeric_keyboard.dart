import 'package:doasdk/doasdk.dart';

class NumericKeyboard extends StatefulWidget {
  final Color? backgroundColor;
  final Color? textColor;
  final ValueChanged<String>? seletedNum;
  final ValueChanged<String>? doneSelected;
  final List? complete;
  final bool? isEdit;
  final int totalText;
  const NumericKeyboard({
    Key? key,
    this.backgroundColor,
    this.isEdit = false,
    this.textColor,
    @required this.seletedNum,
    this.doneSelected,
    this.complete,
    this.totalText = 5,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return NumericKeyboardScreen();
  }
}

class NumericKeyboardScreen extends State<NumericKeyboard> {
  String previousText = '';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return numberPadWidget();
  }

  Container numberPadWidget() {
    return Container(
        decoration: BoxDecoration(
            color: widget.backgroundColor,
            borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(0), topRight: Radius.circular(0))),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GridView(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              childAspectRatio: 2,
              crossAxisSpacing: 20,
            ),
            children: [
              _numberButton(text: "1"),
              _numberButton(text: "2"),
              _numberButton(text: "3"),
              _numberButton(text: "4"),
              _numberButton(text: "5"),
              _numberButton(text: "6"),
              _numberButton(text: "7"),
              _numberButton(text: "8"),
              _numberButton(text: "9"),
              const SizedBox(),
              _numberButton(text: "0"),
              _icon(Icons.backspace, true),
            ],
          ),
        ));
  }

  Widget _numberButton({String? text}) {
    return Material(
      color: Colors.transparent,
      child: Card(
        borderOnForeground: true,
        child: InkWell(
          onTap: () {
            previousText = previousText + text!;
            if (previousText.length > widget.totalText) {
              previousText = previousText.substring(0, widget.totalText);
            }
            widget.seletedNum!(previousText);
          },
          child: Center(
            child: Text(
              text!,
              style: textStyleW700(fontSize: 20),
            ),
          ),
        ),
      ),
    );
  }

  Widget _icon(IconData icon, bool isBackSpace) {
    return IconButton(
        icon: const Icon(Icons.backspace, color: Colors.grey),
        onPressed: () {
          if (isBackSpace) {
            if (previousText.isNotEmpty) {
              var removedText =
                  previousText.substring(0, previousText.length - 1);
              previousText = removedText;
              widget.seletedNum!(removedText);
            } else {
              widget.seletedNum!("");
            }
          } else {
            widget.doneSelected!("Done Selected");
          }
        });
  }
}
