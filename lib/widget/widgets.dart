// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// ignore_for_file: non_constant_identifier_names

import 'package:doasdk/const_path.dart';
import 'package:doasdk/doasdk.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:doasdk/controller/main_controller.dart';
import 'package:doasdk/style/colors.dart';
import 'package:doasdk/style/textstyle.dart';

SafeArea SAFE_AREA({
  @required Widget? child,
  bool top = false,
  bool bottom = false,
}) {
  assert(child != null);
  return SafeArea(
    top: top,
    bottom: bottom,
    child: child!,
  );
}

Scaffold SCAFFOLD({
  @required Widget? body,
  Widget? bottomNavigationBar,
  PreferredSizeWidget? appBar,
  Color? backgroundColor,
}) {
  assert(body != null);
  return Scaffold(
    backgroundColor: backgroundColor ?? Colors.white,
    appBar: appBar,
    body: body,
    bottomNavigationBar: bottomNavigationBar,
  );
}

AppBar APPBAR({
  required void Function()? onPressed,
  @required String? title,
  IconData? icon,
  bool centerTitle = true,
}) {
  // print(Get.currentRoute);
  assert(title != null);
  return AppBar(
    bottom: _progressData != null ? PROGRESS_BAR_DATA(_progressData!) : null,
    centerTitle: centerTitle,
    elevation: Get.currentRoute == "preRegister" ? 0 : 0.5,
    shadowColor:
        Get.currentRoute == "preRegister" ? Colors.white : GREY_BACKGROUND,
    title: Text(
      title!,
      style: textStyleW500(fontSize: 16, fontColor: Colors.black),
    ),
    leading: (onPressed != null && Get.previousRoute.isNotEmpty)
        ? IconButton(
            onPressed: onPressed,
            icon: Icon(
              icon ?? Icons.arrow_back_ios,
              color: Colors.black,
            ))
        : null,
    backgroundColor: Colors.white,
  );
}

ElevatedButton OUTLINE_BUTTON({
  @required Widget? child,
  @required void Function()? onPressed,
  double? radiusCircular,
  MainAxisSize? mainAxisSize,
  Color? borderColor,
}) {
  assert(child != null);
  return ElevatedButton(
      onPressed: onPressed,
      style: ButtonStyle(
          elevation: MaterialStateProperty.all(0),
          shadowColor: MaterialStateProperty.all<Color>(Colors.white),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(radiusCircular ?? 6),
                  side: BorderSide(width: 0.5, color: borderColor ?? ORANGE))),
          backgroundColor: MaterialStateProperty.all<Color>(Colors.white)),
      child: Row(
        mainAxisSize: mainAxisSize ?? MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          child!,
        ],
      ));
}

ElevatedButton BUTTON({
  @required Widget? child,
  @required void Function()? onPressed,
  double? radiusCircular,
  bool isExpanded = true,
}) {
  assert(child != null);
  return ElevatedButton(
      onPressed: onPressed,
      style: ButtonStyle(
          shadowColor: MaterialStateProperty.all<Color>(Colors.white),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(radiusCircular ?? 6))),
          backgroundColor: MaterialStateProperty.all<Color>(
              onPressed == null ? GREY : ORANGE)),
      child: Row(
        mainAxisSize: isExpanded ? MainAxisSize.max : MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          child!,
        ],
      ));
}

int? get _progressData {
  if (Get.currentRoute == "/previewImageTakeCamera") {
    switch (Get.previousRoute) {
      case "/takeKtp":
        return 3;
      case "/selfieAndKtp":
        return 8;
      case "/takeSignature":
        return 9;
      case "/takeNpwp":
        return 10;
      default:
        return null;
    }
  } else {
    switch (Get.currentRoute) {
      case "/inputPhoneNumber":
        return 1;
      case "/accountType":
        return 2;
      case "/preTakeKtp":
        return 3;
      case "/takeKtp":
        return 3;
      case "/registrationForm":
        return 3;
      case "/registrationFormPrivate":
        return 4;
      case "/registrationFormJobDetail":
        return 5;
      case "/registrationFormOfficeBranch":
        return 6;
      case "/livenessVerification":
        return 7;
      case "/selfieAndKtp":
        return 8;
      case "/preTakeSignature":
        return 9;
      case "/takeSignature":
        return 9;
      case "/preTakeNpwp":
        return 10;
      case "/takeNpwp":
        return 10;
      case "/photoValidation":
        return 11;
      case "/dataVerification":
        return 12;
      case "/inputOtpCode":
        return 13;
      case "/createPinAccount":
        return 13;
      case "/completeRegistration":
        return 14;
      default:
        return null;
    }
  }
}

final MainController _mainController = Get.find();
PreferredSize PROGRESS_BAR_DATA(int progressData) {
  return PreferredSize(
      preferredSize: Size(Get.width, 0),
      child: Align(
          alignment: Alignment.centerLeft,
          child: Stack(
            children: [
              Container(width: Get.width, color: GREY_BACKGROUND, height: 5),
              SizedBox(
                  width: Get.width / 14 * progressData,
                  height: 5,
                  child: Obx(() => LinearProgressIndicator(
                        color: ORANGE,
                        value: _mainController.progress.value,
                        backgroundColor: GREY_BACKGROUND,
                      ))),
            ],
          )));
}

Future DIALOG_HELPER(
  String text, {
  Widget? costumText,
  bool useDefaultNavigator = true,
}) {
  return Get.dialog(
      Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              child: SizedBox(
                width: Get.width / 1.2,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      ImageIcon(AssetImage(alertAssets),
                          color: ORANGE, size: 24),
                      const SizedBox(height: 20),
                      costumText ??
                          Text(text, style: textStyleW600(fontSize: 14)),
                      const SizedBox(height: 20),
                      BUTTON(
                          radiusCircular: 999,
                          child: Text(
                            "Oke, mengerti",
                            style: textStyleW600(fontColor: Colors.white),
                          ),
                          onPressed: () =>
                              useDefaultNavigator ? Get.close(1) : Get.back())
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      barrierDismissible: !useDefaultNavigator,
      navigatorKey: useDefaultNavigator ? Get.key : null,
      barrierColor: Colors.black.withOpacity(0.2));
}

Future DIALOG_HELPER_PERMISSION(
  String text, {
  Widget? costumText,
}) {
  return Get.dialog(Center(
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Card(
          child: Container(
            width: Get.width / 1.2,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Icon(Icons.help, color: ORANGE),
                  const SizedBox(width: 10),
                  Expanded(
                    child: Text(text, style: textStyleW600(fontSize: 14)),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    ),
  ));
}

Future<void> BOTTOM_DIALOG_CONFIRMATION({
  String? btnAccTitle,
  String? btnRejectTitle,
  Widget? topTitle,
  void Function()? onAccept,
  void Function()? onReject,
}) async {
  await Get.bottomSheet(
      Container(
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
        child: Padding(
          padding: const EdgeInsets.all(32.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Card(
                color: BLUE_LIGHT,
                elevation: 0,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 17.67, vertical: 16),
                  child: Row(
                    children: [
                      const Icon(Icons.help, color: BLUE_TEXT),
                      const SizedBox(width: 9.67),
                      topTitle ??
                          Text("Top Title",
                              style: textStyleW600(
                                  fontSize: 12, fontColor: BLUE_TEXT)),
                    ],
                  ),
                ),
              ),
              BUTTON(
                  radiusCircular: 999,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: Text(
                      btnAccTitle ?? "Accept",
                      style:
                          textStyleW600(fontSize: 16, fontColor: Colors.white),
                    ),
                  ),
                  onPressed: onAccept),
              const SizedBox(height: 16),
              OUTLINE_BUTTON(
                  radiusCircular: 999,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: Text(btnRejectTitle ?? "Reject",
                        style: textStyleW600(fontSize: 16, fontColor: ORANGE)),
                  ),
                  onPressed: onReject)
            ],
          ),
        ),
      ),
      isDismissible: false);
}
