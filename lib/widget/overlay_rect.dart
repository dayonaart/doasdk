import 'package:doasdk/style/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OverlayRect extends StatelessWidget {
  const OverlayRect({Key? key, this.height, this.width}) : super(key: key);
  final double? height;
  final double? width;
  @override
  Widget build(BuildContext context) {
    return CustomPaint(
        size: Get.size,
        painter: OverlayPainter(
            top: (Get.height / 7.5), height: height, width: width));
  }
}

class OverlayPainter extends CustomPainter {
  final double? height;
  final double? width;
  final double? top;
  OverlayPainter({
    this.height,
    this.width,
    this.top,
  });
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()..color = GREY_CAMERA.withOpacity(0.8);

    canvas.drawPath(
        Path.combine(
          PathOperation.difference, //simple difference of following operations
          //bellow draws a rectangle of full screen (parent) size
          Path()..addRect(Rect.fromLTWH(0, 0, size.width, size.height)),
          //bellow clips out the circular rectangle with center as offset and dimensions you need to set
          Path()
            ..addRRect(RRect.fromRectAndRadius(
                Rect.fromLTRB(16, top ?? 0, (size.width - 16), height ?? 200),
                const Radius.circular(8)))
            ..close(),
        ),
        paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
