// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:doasdk/controller/create_pin_account_controller.dart';
import 'package:doasdk/doasdk.dart';
import '../widget/numeric_keyboard.dart';

class CreatePinAccount extends StatelessWidget {
  CreatePinAccount({Key? key}) : super(key: key);
  final _controller = Get.put(CreatePinAccountController());
  @override
  Widget build(BuildContext context) {
    return SAFE_AREA(
        child: SCAFFOLD(
            appBar: APPBAR(
                onPressed: () => Get.close(1), title: "Buat PIN Kartu Debit"),
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 16),
                    const CreatePinHeader(),
                    const SizedBox(height: 24),
                    Text("PIN Kartu Debit", style: textStyleW600(fontSize: 14)),
                    const SizedBox(height: 32),
                    InputOtpField(),
                    Obx(() {
                      if (_controller.pinController(true).value.length != 6) {
                        return Text(
                          "PIN Wajib diisi",
                          style: textStyleW500(fontColor: Colors.red),
                        );
                      } else {
                        return Container();
                      }
                    }),
                    const SizedBox(height: 24),
                    Text("Konfirmasi PIN Kartu Debit",
                        style: textStyleW600(fontSize: 14)),
                    const SizedBox(height: 4),
                    Text("Pastikan PIN sama dengan yang Anda buat diatas.",
                        style: textStyleW500(fontSize: 14)),
                    const SizedBox(height: 32),
                    InputOtpField(isPin: false),
                    Obx(() {
                      if (_controller.pinController(false).value.length != 6) {
                        return Text(
                          "Konfirmasi PIN Wajib diisi",
                          style: textStyleW500(fontColor: Colors.red),
                        );
                      } else {
                        return Container();
                      }
                    }),
                  ],
                ),
              ),
            ),
            bottomNavigationBar: KeyboardButtonWidget()));
  }
}

class CreatePinHeader extends StatelessWidget {
  const CreatePinHeader({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      color: BLUE_LIGHT,
      margin: const EdgeInsets.all(0),
      elevation: 0,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 13.25, vertical: 16),
        child: Row(
          children: [
            ImageIcon(AssetImage(lampAssets), color: BLUE_TEXT),
            const SizedBox(width: 12),
            Expanded(
                child: Text.rich(
              TextSpan(children: [
                const TextSpan(text: "Pastikan"),
                TextSpan(
                    text:
                        "PIN kartu debit akan digunakan untuk melakukan aktivasi BNI Mobile Banking. Masukan 6 angka yang",
                    style: textStyleW600()),
                const TextSpan(text: "untuk pengiriman OTP melalui SMS atau "),
                TextSpan(
                    text:
                        " dapat Anda ingat dan jangan bagikan PIN ini ke siapapun.",
                    style: textStyleW600()),
              ]),
              style: textStyleW500(fontSize: 12, fontColor: BLUE_TEXT),
            ))
          ],
        ),
      ),
    );
  }
}

class InputOtpField extends StatelessWidget {
  final bool isPin;

  InputOtpField({
    Key? key,
    this.isPin = true,
  }) : super(key: key);
  final CreatePinAccountController _controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List.generate(7, (i) {
        if (i == 6) {
          return Expanded(
            child: IconButton(
                onPressed: _controller.changeVisibility(isPin),
                icon: Obx(() {
                  return Icon(
                    _controller.pinVisibility(isPin).value
                        ? Icons.visibility_off
                        : Icons.visibility,
                    color: ORANGE,
                  );
                })),
          );
        }
        return Expanded(
          child: Padding(
              padding: const EdgeInsets.only(right: 8),
              child: OtpField(isPin: isPin, i: i)),
        );
      }),
    );
  }
}

class OtpField extends StatelessWidget {
  OtpField({
    Key? key,
    required this.isPin,
    required this.i,
  }) : super(key: key);

  final CreatePinAccountController _controller = Get.find();
  final bool isPin;
  final int i;

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Container(
        width: 40,
        decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(
                  width: 2, color: _controller.borderPinColor(isPin, i))),
        ),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: Builder(builder: (context) {
              try {
                return Text(_controller.pinController(isPin).value[i],
                    style: textStyleW600(fontSize: 20));
              } catch (e) {
                return const SizedBox();
              }
            }),
          ),
        ),
      );
    });
  }
}

class KeyboardButtonWidget extends StatelessWidget {
  final CreatePinAccountController _controller = Get.find();

  KeyboardButtonWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Obx(() {
            return BUTTON(
                radiusCircular: 999,
                onPressed: _controller.next(),
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 14),
                  child: Text(
                    "Lanjut",
                    style: textStyleW600(fontSize: 14, fontColor: Colors.white),
                  ),
                ));
          }),
        ),
        const SizedBox(height: 32),
        NumericKeyboard(
          totalText: 12,
          seletedNum: _controller.seletedNum(),
          backgroundColor: GREY_BACKGROUND,
        ),
      ],
    );
  }
}
