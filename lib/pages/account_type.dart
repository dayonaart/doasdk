// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:carousel_slider/carousel_slider.dart';
import 'package:doasdk/controller/account_type_controller.dart';
import 'package:doasdk/doasdk.dart';
import 'dart:math' as math;

class AccountType extends StatelessWidget {
  AccountType({Key? key}) : super(key: key);
  final _controller = Get.put(AccountTypeController());
  @override
  Widget build(BuildContext context) {
    return SAFE_AREA(
        child: SCAFFOLD(
      appBar: APPBAR(onPressed: () => Get.back(), title: "Pilih Rekening"),
      body: SingleChildScrollView(
        controller: _controller.scController,
        child: Column(
          children: [
            AccountHeader(),
            AccountList(),
            const SizedBox(height: 14.76),
            AccountTitle(),
            const SizedBox(height: 24),
            AccountFeature(),
          ],
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 39),
        child: BUTTON(
            radiusCircular: 999,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 12),
              child: Text(
                "Pilih",
                style: textStyleW600(fontSize: 16, fontColor: Colors.white),
              ),
            ),
            onPressed: _controller.next()),
      ),
    ));
  }
}

class AccountFeature extends StatelessWidget {
  AccountFeature({Key? key}) : super(key: key);
  final AccountTypeController _controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          Column(
            children: [
              CardFeature(),
              SizeTransition(
                  axisAlignment: 1.0,
                  sizeFactor: _controller.animation.value,
                  child: Column(
                    children: [
                      CardBenefit(),
                      InterestRate(),
                      FeatureAndCost(),
                    ],
                  )),
            ],
          ),
          ShowFullDescription()
        ],
      ),
    );
  }
}

class CardFeature extends StatelessWidget {
  final AccountTypeController _controller = Get.find();

  CardFeature({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Fitur Kartu",
              style: textStyleW600(fontSize: 14, fontColor: ORANGE)),
          const SizedBox(height: 20),
          Column(
            children: List.generate(_controller.featureLength, (i) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 12),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                        child: Text(
                      FeatureTitleList.values[i].title,
                      style: textStyleW500(fontSize: 14),
                    )),
                    Expanded(
                        child: Text(
                      _controller.featureValue[i],
                      style: textStyleW600(fontSize: 14),
                      textAlign: TextAlign.end,
                    ))
                  ],
                ),
              );
            }),
          ),
        ],
      );
    });
  }
}

class CardBenefit extends StatelessWidget {
  final AccountTypeController _controller = Get.find();

  CardBenefit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Keuntungan & Manfaat",
              style: textStyleW600(fontSize: 14, fontColor: ORANGE)),
          const SizedBox(height: 20),
          Text(_controller.expandDesc.value,
              style: textStyleW500(fontSize: 14)),
          const SizedBox(height: 20),
        ],
      );
    });
  }
}

class InterestRate extends StatelessWidget {
  final AccountTypeController _controller = Get.find();

  InterestRate({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Suku Bunga:",
              style: textStyleW600(fontSize: 14, fontColor: ORANGE)),
          const SizedBox(height: 20),
          Column(
            children: List.generate(_controller.expandDesc2.length, (i) {
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Text(_controller.expandDesc2[i],
                          style: textStyleW500(fontSize: 14)),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(
                        _controller.expandDesc2Value[i],
                        style: textStyleW500(fontSize: 14),
                        textAlign: TextAlign.end,
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
          const SizedBox(height: 20),
        ],
      );
    });
  }
}

class FeatureAndCost extends StatelessWidget {
  final AccountTypeController _controller = Get.find();

  FeatureAndCost({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Fitur & Biaya Lainnya : ",
              style: textStyleW600(fontSize: 14, fontColor: ORANGE)),
          const SizedBox(height: 20),
          Column(
            children: List.generate(_controller.expandDesc3.length, (i) {
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Text(_controller.expandDesc3[i],
                          style: textStyleW500(fontSize: 14)),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(
                        _controller.expandDesc3Value[i],
                        style: textStyleW500(fontSize: 14),
                        textAlign: TextAlign.end,
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
        ],
      );
    });
  }
}

class AccountTitle extends StatelessWidget {
  AccountTitle({Key? key}) : super(key: key);

  final AccountTypeController _controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(_controller.accountAsset.length, (i) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 4),
              child: Obx(() {
                return Container(
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _controller.carouselIndexColor(i)),
                  height: 8,
                  width: 8,
                );
              }),
            );
          }),
        ),
        const SizedBox(height: 16),
        Obx(() => Text(
              _controller.accountCardName.value,
              style: textStyleW600(fontSize: 16),
            ))
      ],
    );
  }
}

class AccountHeader extends StatelessWidget {
  AccountHeader({Key? key}) : super(key: key);
  final AccountTypeController _controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 32),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Jenis Tabungan",
            style: textStyleW600(fontSize: 14),
          ),
          const SizedBox(height: 27.75),
          BUTTON(
              radiusCircular: 999,
              isExpanded: false,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                child: Obx(() => Text(
                      _controller.accountCardName.value,
                      style: textStyleW600(fontSize: 14),
                    )),
              ),
              onPressed: () {}),
          const SizedBox(height: 24.25),
          Obx(() => Text(_controller.accountCardHeader.value,
              style: textStyleW500(fontSize: 14))),
          const SizedBox(height: 23.69),
        ],
      ),
    );
  }
}

class AccountList extends StatelessWidget {
  AccountList({Key? key}) : super(key: key);

  final AccountTypeController _controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
        items: List.generate(_controller.accountAsset.length, (i) {
          return Obx(() {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Opacity(
                opacity: _controller.corouselIndex.value == i ? 1 : 0.4,
                child: Image.asset(
                  _controller.accountAsset[i],
                  filterQuality: FilterQuality.high,
                ),
              ),
            );
          });
        }),
        options: CarouselOptions(
            height: 137,
            // padEnds: true,
            viewportFraction: 0.55,
            enableInfiniteScroll: true,
            onPageChanged: _controller.onPageChanged()));
  }
}

class ShowFullDescription extends StatelessWidget {
  final AccountTypeController _controller = Get.find();

  ShowFullDescription({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
        padding: const EdgeInsets.all(0),
        onPressed: _controller.showFullFeature(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Obx(() {
              return Text(
                _controller.showFullFeatureBtnTitle.value,
                style: textStyleW600(fontSize: 14, fontColor: ORANGE),
              );
            }),
            const SizedBox(width: 14.41),
            Obx(() {
              return Transform.rotate(
                  angle: _controller.showFullFeatureArrowAngle.value / math.pi,
                  child: const Icon(
                    Icons.arrow_back_ios_new,
                    color: ORANGE,
                    size: 20,
                  ));
            })
          ],
        ));
  }
}
