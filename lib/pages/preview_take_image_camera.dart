// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'dart:io';
import 'package:doasdk/controller/main_controller.dart';
import 'package:doasdk/doasdk.dart';

class PreviewTakeImage extends StatelessWidget {
  final MainController _mController = Get.find();
  PreviewTakeImage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SAFE_AREA(
        child: SCAFFOLD(
      backgroundColor: BLUE_LIGHT,
      appBar: APPBAR(onPressed: null, title: _appBarTitle),
      body: Obx(() {
        if (_mController.retakeFile.value != null) {
          return Image.file(_mController.retakeFile.value!);
        }
        return Image.file(_previewImage.value!);
      }),
    ));
  }

  Rx<File?> get _previewImage {
    switch (Get.previousRoute) {
      case "/takeKtp":
        return _mController.ktpFile;
      case "/selfieAndKtp":
        return _mController.selfieFile;
      case "/takeSignature":
        return _mController.signatureFile;
      case "/takeNpwp":
        return _mController.npwpFile;
      default:
        return Rx(null);
    }
  }

  String get _appBarTitle {
    switch (Get.previousRoute) {
      case "/takeKtp":
        return "Registrasi";
      case "/selfieAndKtp":
        return "Foto Selfie dengan KTP";
      case "/takeSignature":
        return "Foto Tanda Tangan";
      case "/takeNpwp":
        return "Foto NPWP";
      default:
        return "";
    }
  }
}
