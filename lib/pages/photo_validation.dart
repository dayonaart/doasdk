// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'dart:typed_data';
import 'package:doasdk/widget/dashed_rect.dart';
import 'package:doasdk/controller/main_controller.dart';
import 'package:doasdk/controller/photo_validation_controller.dart';
import 'package:doasdk/doasdk.dart';
// ignore_for_file: unused_field

class PhotoValidation extends StatelessWidget {
  PhotoValidation({Key? key}) : super(key: key);
  final _controller = Get.put(PhotoValidationController());
  final MainController _mController = Get.find();

  @override
  Widget build(BuildContext context) {
    return SAFE_AREA(
        child: SCAFFOLD(
            appBar:
                APPBAR(onPressed: () => Get.close(1), title: "Validasi Foto"),
            body: SingleChildScrollView(
                child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const PhotoValidationHeader(),
                  const SizedBox(height: 16),
                  FaceAndKtpWidget(),
                  const SizedBox(height: 32),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SignatureWidget(),
                      const SizedBox(width: 10),
                      NpwpWidget(),
                    ],
                  ),
                ],
              ),
            )),
            bottomNavigationBar: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 39),
              child: BUTTON(
                  radiusCircular: 999,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: Text(
                      "Lanjut",
                      style:
                          textStyleW600(fontSize: 16, fontColor: Colors.white),
                    ),
                  ),
                  onPressed: _controller.next()),
            )));
  }
}

class FaceAndKtpWidget extends StatelessWidget {
  final MainController _mController = Get.find();

  FaceAndKtpWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return FutureBuilder<Uint8List?>(
          future: _mController.cropImage(_mController.selfieFile.value,
              useOringalImage: true),
          builder: (context, snap) {
            if (snap.hasData) {
              return FileViewer("Foto Selfie dengan KTP", snap.data!);
            } else {
              return Container();
            }
          });
    });
  }
}

class NpwpWidget extends StatelessWidget {
  final MainController _mController = Get.find();
  NpwpWidget({Key? key}) : super(key: key);
  final PhotoValidationController _controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return FutureBuilder<Uint8List?>(
          future: _mController.cropImage(_mController.npwpFile.value),
          builder: (context, snap) {
            if (snap.hasData) {
              return Expanded(
                child: FileViewer(
                  "NPWP (Opsional)",
                  snap.data!,
                  boxHeight: 128,
                ),
              );
            } else {
              return Expanded(
                child: GestureDetector(
                  onTap: _controller.changeData("NPWP (Opsional)"),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("NPWP (Opsional)",
                          style: textStyleW600(fontSize: 14)),
                      const SizedBox(height: 21.38),
                      Stack(
                        alignment: Alignment.center,
                        children: [
                          const DashedRect(
                              strokeWidth: 2,
                              height: 128,
                              width: 181,
                              color: GREY),
                          Column(
                            children: [
                              const Icon(Icons.camera_alt_outlined,
                                  color: GREY),
                              Text(
                                "Upload Foto NPWP",
                                style: textStyleW600(
                                    fontSize: 12, fontColor: GREY),
                              ),
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              );
            }
          });
    });
  }
}

class SignatureWidget extends StatelessWidget {
  final MainController _mController = Get.find();

  SignatureWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return FutureBuilder<Uint8List?>(
          future: _mController.cropImage(_mController.signatureFile.value),
          builder: (context, snap) {
            if (snap.hasData) {
              return Expanded(
                child: FileViewer(
                  "Tanda Tangan",
                  snap.data!,
                  boxHeight: 128,
                ),
              );
            } else {
              return Container();
            }
          });
    });
  }
}

class FileViewer extends StatelessWidget {
  final String? title;
  final Uint8List uint8list;
  final double boxHeight;

  FileViewer(
    this.title,
    this.uint8list, {
    Key? key,
    this.boxHeight = 200,
  }) : super(key: key);
  final PhotoValidationController _controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(title!, style: textStyleW600(fontSize: 14)),
        Center(
          child: Column(
            children: [
              const SizedBox(height: 21.38),
              ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: Image.memory(uint8list,
                      height: boxHeight, width: 181, fit: BoxFit.cover)),
              const SizedBox(height: 16),
              OUTLINE_BUTTON(
                  radiusCircular: 999,
                  mainAxisSize: MainAxisSize.min,
                  child: Row(
                    children: [
                      ImageIcon(AssetImage(penAssets), color: ORANGE),
                      const SizedBox(width: 6.27),
                      Text(
                        "Ubah",
                        style: textStyleW500(fontSize: 14, fontColor: ORANGE),
                      )
                    ],
                  ),
                  onPressed: _controller.changeData(title))
            ],
          ),
        ),
      ],
    );
  }
}

class PhotoValidationHeader extends StatelessWidget {
  const PhotoValidationHeader({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Card(
          color: BLUE_LIGHT,
          elevation: 0,
          margin: const EdgeInsets.all(0),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 16, horizontal: 13.67),
            child: Row(
              children: [
                ImageIcon(AssetImage(iAssets), color: BLUE_TEXT),
                const SizedBox(width: 13.67),
                Expanded(
                    child: Text.rich(TextSpan(children: [
                  TextSpan(
                      text: "Pastikan foto yang anda ambil ",
                      style: textStyleW500(fontSize: 12, fontColor: BLUE_TEXT)),
                  TextSpan(
                      text: "sudah sesuai dan sudah fokus (tidak blur).",
                      style: textStyleW600(fontSize: 12, fontColor: BLUE_TEXT)),
                ]))),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
