// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:doasdk/controller/data_verification_controller.dart';
import 'package:doasdk/doasdk.dart';

class DataVerification extends StatelessWidget {
  DataVerification({Key? key}) : super(key: key);
  final _controller = Get.put(DataVerificationController());
  @override
  Widget build(BuildContext context) {
    return SAFE_AREA(
      child: SCAFFOLD(
        appBar: APPBAR(onPressed: () => Get.back(), title: "Verifikasi"),
        body: SingleChildScrollView(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(children: [
            const SizedBox(height: 40),
            CircleAvatar(
                radius: 152 / 2,
                backgroundColor: PINK_LIGHT,
                child: Image.asset(phoneHandAssets)),
            const SizedBox(height: 24),
            Builder(builder: (context) {
              try {
                return Text(
                    "Verifikasi Nomor ${readUserData()?.number?.replaceRange(3, (readUserData()?.number ?? "000000").length - 4, "******")}",
                    style: textStyleW600(fontSize: 16));
              } catch (e) {
                return Text("Maaf no hp harus diisi untuk melanjutkan",
                    style: textStyleW600(fontSize: 16));
              }
            }),
            const SizedBox(height: 8),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: Text(
                "Pastikan nomor telepon sudah sesuai untuk mendapatkan OTP.",
                style: textStyleW500(fontSize: 14),
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(height: 24),
            Card(
              color: BLUE_LIGHT,
              margin: const EdgeInsets.all(0),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              elevation: 0,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 13.25, vertical: 16),
                child: Column(
                  children: List.generate(3, (i) {
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 16),
                      child: Row(
                        children: [
                          _controller.helperDescriptionIcon(i),
                          const SizedBox(width: 12),
                          Expanded(child: _controller.helperDescription(i))
                        ],
                      ),
                    );
                  }),
                ),
              ),
            ),
            const SizedBox(height: 119),
            Text.rich(
              TextSpan(children: [
                const TextSpan(text: "Pastikan keamanan akun WhatsApp Anda.\n"),
                TextSpan(
                    text: "Baca lebih lanjut",
                    style: textStyleW600(fontColor: Colors.red)),
                const TextSpan(text: " untuk info keamanan verifikasi.")
              ]),
              style: textStyleW600(fontSize: 12),
              textAlign: TextAlign.center,
            ),
          ]),
        )),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.only(right: 16, left: 16, bottom: 39),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              BUTTON(
                  radiusCircular: 999,
                  onPressed: _controller.sentWhatsappVerificationCode(),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 14),
                    child: Row(
                      children: [
                        ImageIcon(AssetImage(whatsappAssets)),
                        const SizedBox(width: 10),
                        Text(
                          "Kirim Melalui Whatapp",
                          style: textStyleW600(
                              fontSize: 14, fontColor: Colors.white),
                        ),
                      ],
                    ),
                  )),
              const SizedBox(height: 16),
              OUTLINE_BUTTON(
                  radiusCircular: 999,
                  onPressed: _controller.sentSmsVerificationCode(),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 14),
                    child: Row(
                      children: [
                        ImageIcon(AssetImage(smsAssets), color: ORANGE),
                        const SizedBox(width: 10),
                        Text(
                          "Kirim melalui SMS",
                          style: textStyleW600(fontSize: 14, fontColor: ORANGE),
                        ),
                      ],
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
