// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:doasdk/controller/complete_registration_controller.dart';
import 'package:doasdk/doasdk.dart';
import 'package:screenshot/screenshot.dart';

class CompleteRegistration extends StatelessWidget {
  CompleteRegistration({Key? key}) : super(key: key);
  final _controller = Get.put(CompleteRegistrationController());
  @override
  Widget build(BuildContext context) {
    return SAFE_AREA(
        child: SCAFFOLD(
            appBar: APPBAR(onPressed: null, title: "Selesai"),
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Screenshot(
                        controller: _controller.screenshotController,
                        child: Material(child: CompleteBody())),
                    BUTTON(
                        radiusCircular: 6,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 15),
                          child: Row(
                            children: [
                              ImageIcon(AssetImage(accountAssets)),
                              const SizedBox(width: 12.74),
                              Text(
                                "Daftar akun BNI Agen46",
                                style: textStyleW600(
                                    fontSize: 14, fontColor: Colors.white),
                              )
                            ],
                          ),
                        ),
                        onPressed: _controller.next()),
                    const SizedBox(height: 16),
                    OUTLINE_BUTTON(
                        radiusCircular: 6,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 15),
                          child: Row(
                            children: [
                              ImageIcon(AssetImage(downloadAssets),
                                  color: ORANGE),
                              const SizedBox(width: 12.74),
                              Text(
                                "Simpan bukti pendaftaran",
                                style: textStyleW600(
                                    fontSize: 14, fontColor: ORANGE),
                              )
                            ],
                          ),
                        ),
                        onPressed: _controller.saveCompleteRegistration()),
                    const SizedBox(height: 36),
                  ],
                ),
              ),
            )));
  }
}

class CompleteBody extends StatelessWidget {
  final CompleteRegistrationController _controller = Get.find();

  CompleteBody({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 16),
        Container(
          height: 152,
          width: 152,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                  image: AssetImage(completeRegistrationAssets))),
        ),
        const SizedBox(height: 24),
        Text("Pembukaan Rekening Berhasil", style: textStyleW600(fontSize: 16)),
        const SizedBox(height: 8),
        Text("Nomor Rekening", style: textStyleW500(fontSize: 14)),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("${_controller.callbackData.accountNumber}",
                style: textStyleW600(fontSize: 20, fontColor: ORANGE)),
            IconButton(
                onPressed: _controller.copyAccountNumber(),
                icon: const Icon(Icons.copy_outlined, color: ORANGE))
          ],
        ),
        const SizedBox(height: 24),
        const HeaderDescription(),
        const SizedBox(height: 24),
        DetailAccount(
          title: "No. Kartu Virtual",
          value: "${_controller.callbackData.virtualNumber}",
        ),
        DetailAccount(
          title: "Nama Nasabah",
          value: "${_controller.callbackData.customersName}",
        ),
        DetailAccount(
          title: "Jenis Produk",
          value: "${_controller.callbackData.cardType}",
        ),
        DetailAccount(
            title: "Tanggal Pembuatan",
            value: "${_controller.callbackData.createdDate}"),
        DetailAccount(
          title: "Waktu Pembuatan",
          value: "${_controller.callbackData.createdTime}",
        ),
        DetailAccount(
          title: "Kantor Cabang",
          value: "${_controller.callbackData.officeBranch}",
        ),
        DetailAccount(
          title: "Keterangan",
          value: "${_controller.callbackData.cardDetail}",
        ),
        const SizedBox(height: 52),
      ],
    );
  }
}

class DetailAccount extends StatelessWidget {
  final String title;
  final String value;
  const DetailAccount({Key? key, required this.title, required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          border: Border.symmetric(
              horizontal: BorderSide(width: 1, color: GREY_BACKGROUND))),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Text(title, style: textStyleW500(fontSize: 14)),
            ),
            Expanded(
              child: Text(
                value,
                style: textStyleW600(fontSize: 14, fontColor: GREY),
                textAlign: TextAlign.end,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class HeaderDescription extends StatelessWidget {
  const HeaderDescription({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      color: BLUE_LIGHT,
      elevation: 0,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ImageIcon(AssetImage(iAssets), color: BLUE_TEXT),
            const SizedBox(width: 9.67),
            Expanded(
                child: Text.rich(TextSpan(children: [
              TextSpan(
                  text: "Segera lakukan setoran awal ",
                  style: textStyleW500(fontSize: 12, fontColor: BLUE_TEXT)),
              TextSpan(
                  text:
                      "minimal Rp150.000,- paling lambat sebelum 10 April 2022 ",
                  style: textStyleW600(fontSize: 12, fontColor: BLUE_TEXT)),
              TextSpan(
                  text:
                      "agar rekening anda tidak tertutup otomatis.\n\nSelanjutnya dapat mengunjungi kantor cabang BNI terdekat untuk mendapatkan kartu debit fisik atau buku tabungan",
                  style: textStyleW500(fontSize: 12, fontColor: BLUE_TEXT))
            ]))),
          ],
        ),
      ),
    );
  }
}
