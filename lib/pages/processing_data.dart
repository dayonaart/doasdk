// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:doasdk/controller/processing_data_controller.dart';
import 'package:doasdk/doasdk.dart';

class ProcessingData extends StatelessWidget {
  ProcessingData({Key? key}) : super(key: key);
  final _controller = Get.put(ProcessingDataController());
  @override
  Widget build(BuildContext context) {
    return SAFE_AREA(
        child: SCAFFOLD(
            body: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Stack(
            alignment: Alignment.center,
            children: [
              Obx(() {
                return SizedBox(
                  height: 152,
                  width: 152,
                  child: CircularProgressIndicator(
                    strokeWidth: 8,
                    color: ORANGE,
                    value: _controller.progressAnimation.value,
                    semanticsLabel: 'progress indicator',
                  ),
                );
              }),
              Padding(
                padding: const EdgeInsets.all(36.0),
                child: Container(
                  height: 80,
                  width: 80,
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle, color: ORANGE),
                  child: Center(
                      child: ImageIcon(
                    AssetImage(processingDataAssets),
                    color: Colors.white,
                  )),
                ),
              ),
            ],
          ),
          const SizedBox(height: 20),
          Text("Tetap berada pada halaman ini",
              style: textStyleW600(fontSize: 20)),
          const SizedBox(height: 16),
          Text(
            "Mohon menunggu, kami sedang mempersiapkan rekening Anda. Proses akan berjalan kurang lebih selama 5 menit.",
            style: textStyleW500(fontSize: 14),
            textAlign: TextAlign.center,
          )
        ],
      ),
    )));
  }
}
