// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:doasdk/controller/input_otp_code_controller.dart';
import 'package:doasdk/controller/main_controller.dart';
import 'package:doasdk/doasdk.dart';
import '../widget/numeric_keyboard.dart';

class InputOtpCode extends StatelessWidget {
  InputOtpCode({Key? key}) : super(key: key);
  // ignore: unused_field
  final _controller = Get.put(InputOtpCodeController());
  final MainController _mController = Get.find();
  @override
  Widget build(BuildContext context) {
    return SAFE_AREA(
        child: SCAFFOLD(
      appBar: APPBAR(onPressed: () => Get.close(1), title: "Kode OTP"),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 16),
                  const OtpHeader(),
                  const SizedBox(height: 24),
                  Text("Masukkan 6 digit Kode OTP",
                      style: textStyleW600(fontSize: 14)),
                  const SizedBox(height: 6),
                  Builder(builder: (context) {
                    if (readUserData()?.number != null) {
                      return Text(
                          "Kode OTP dikirimkan ke nomor +62 ${readUserData()?.number}",
                          style: textStyleW500(fontSize: 14));
                    }
                    return Text(
                        "Maaf no hp harus diisi untuk agar dapat melanjutkan",
                        style: textStyleW600(fontSize: 16));
                  }),
                  const SizedBox(height: 32),
                  InputOtpField(),
                  const SizedBox(height: 24),
                  ResendOtp(),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: KeyboardButtonWidget(),
    ));
  }
}

class InputOtpField extends StatelessWidget {
  final InputOtpCodeController _controller = Get.find();

  InputOtpField({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List.generate(5, (i) {
        return Padding(
            padding: const EdgeInsets.only(right: 8),
            child: Obx(() {
              try {
                return Container(
                  width: 40,
                  decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            width: 2,
                            color: _controller.otpTextController.value.length ==
                                    (i + 1)
                                ? ORANGE
                                : GREY_BACKGROUND)),
                  ),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                      child: Builder(builder: (context) {
                        try {
                          return Text(_controller.otpTextController.value[i],
                              style: textStyleW600(fontSize: 20));
                        } catch (e) {
                          return const SizedBox();
                        }
                      }),
                    ),
                  ),
                );
              } catch (e) {
                return const SizedBox();
              }
            }));
      }),
    );
  }
}

class ResendOtp extends StatelessWidget {
  final InputOtpCodeController _controller = Get.find();
  final MainController _mController = Get.find();

  ResendOtp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Obx(() {
        if (_mController.time.value == "0") {
          return TextButton(
              onPressed: _controller.resendOtp(),
              child: Text(
                "Kirim Ulang OTP",
                style: textStyleW600(
                    fontSize: 12,
                    fontColor: ORANGE,
                    textDecoration: TextDecoration.underline),
              ));
        }
        return Text(
          "Kirim Ulang OTP (${_mController.time.value})",
          style: textStyleW600(
              fontSize: 12,
              fontColor: GREY,
              textDecoration: TextDecoration.underline),
        );
      }),
    );
  }
}

class KeyboardButtonWidget extends StatelessWidget {
  final InputOtpCodeController _controller = Get.find();

  KeyboardButtonWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: BUTTON(
              radiusCircular: 999,
              onPressed: _controller.next(),
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 14),
                child: Text(
                  "Lanjut",
                  style: textStyleW600(fontSize: 14, fontColor: Colors.white),
                ),
              )),
        ),
        const SizedBox(height: 32),
        NumericKeyboard(
          seletedNum: _controller.seletedNum(),
          backgroundColor: GREY_BACKGROUND,
        ),
      ],
    );
  }
}

class OtpHeader extends StatelessWidget {
  const OtpHeader({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      color: BLUE_LIGHT,
      margin: const EdgeInsets.all(0),
      elevation: 0,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 13.25, vertical: 16),
        child: Row(
          children: [
            ImageIcon(AssetImage(lampAssets), color: BLUE_TEXT),
            const SizedBox(width: 12),
            Expanded(
                child: Text.rich(
              TextSpan(children: [
                const TextSpan(text: "Pastikan"),
                TextSpan(
                    text: "nomor handphone Anda aktif dan memiliki pulsa ",
                    style: textStyleW600()),
                const TextSpan(text: "untuk pengiriman OTP melalui SMS atau "),
                TextSpan(
                    text: "terhubung dengan jaringan internet ",
                    style: textStyleW600()),
                const TextSpan(text: "untuk pengiriman OTP melalui WhatsApp.")
              ]),
              style: textStyleW500(fontSize: 12, fontColor: BLUE_TEXT),
            ))
          ],
        ),
      ),
    );
  }
}
