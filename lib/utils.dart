// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'dart:math';

import 'package:doasdk/doasdk.dart';
import 'package:doasdk/model/complete_registration_model.dart';
import 'package:doasdk/model/user_data_model.dart';
import 'package:get_storage/get_storage.dart';

final _box = GetStorage();

extension PersonNameValidator on String {
  bool isNotValidPersonName() {
    return RegExp(r'[{}+!@#$%^&*()?><,./\;@=_0-9-]').hasMatch(this);
  }
}

extension OnlyTextAndNumberValidator on String {
  bool isValidTextAndNumber() {
    return RegExp(r'[{}+!@#$%^&*()?><,./\;@=_-]').hasMatch(this);
  }
}

extension EmailValidator on String {
  bool isValidEmail() {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(this);
  }
}

extension PhoneValidator on String {
  bool isValidPhone() {
    return RegExp(r'(^(?:[+0]9)?[0-9]{10,12}$)').hasMatch(this);
  }
}

extension NpwpValidator on String {
  bool isValidNpwp() {
    return RegExp(r'(^(?:[+0]9)?[0-9]{16}$)').hasMatch(this);
  }
}

void printWrapped(String text) {
  final pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
  // ignore: avoid_print
  pattern.allMatches(text).forEach((match) => print(match.group(0)));
}

String randomNumber(int length) {
  var _rnd = Random();
  var _l = List.generate(length, (_) => _rnd.nextInt(9));
  return _l.join();
}

String get randomAccountNumber {
  var _rnd = Random();
  var _l =
      List.generate(4, (index) => List.generate(4, (_) => _rnd.nextInt(9)));
  return "${_l[0].join()}-${_l[1].join()}-${_l[2].join()}-${_l[3].join()}";
}

void Function(int, String)? onInterruptCallback;
void Function(int, CompleteRegistrationModel?)? onCompleteCallback;
CompleteRegistrationModel? completeRegistrationModel;
BuildContext? clientContext;
Future<void> initStorage() async {
  if (!(await GetStorage.init())) {
    DIALOG_HELPER("Lokasi penyimpanan tidak diizinkan");
  }
}

Future<void> writeUserData(Map<String, dynamic> data) async {
  await _box.write("user_data", data);
}

UserDataModel? readUserData() {
  try {
    var _data = _box.read<Map<String, dynamic>>("user_data");
    return UserDataModel.fromJson(_data!);
  } catch (e) {
    return null;
  }
}

Future<void> clearUserData() async {
  await _box.remove("user_data");
}
// Future<bool> saveFile(
//   Uint8List uint8list,
//   String fileName,
// ) async {
//   var _dPath = await _documentPath;
//   var _savedFile = await File("$_dPath/$fileName")
//       .writeAsBytes(uint8list, mode: FileMode.append);
//   return _savedFile.exists();
// }

// Future<void> deleteFile(String fileName) async {
//   var _dPath = await _documentPath;
//   var _file = await File("$_dPath/$fileName").delete();
//   if (await _file.exists()) {
//     DIALOG_HELPER("Can't delete cache file");
//   }
// }

// Future<File?> readFile(String fileName) async {
//   var _dPath = await _documentPath;
//   var _file = File("$_dPath/$fileName");
//   return _file;
// }

// Future<String?> get _documentPath async {
//   final directory = await getApplicationDocumentsDirectory();
//   return directory.path;
// }
