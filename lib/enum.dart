// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

enum OnBoardingWord {
  tungguApalagi,
  nikmatiKemudahanDenganBNIAgen46,
  melayaniPalingDekat,
  belumPunyaAkunBNIAgen46,
  daftarSekarang,
  masuk
}

extension OnBoardingText on OnBoardingWord {
  String get text {
    switch (this) {
      case OnBoardingWord.tungguApalagi:
        return "Tunggu Apalagi!";
      case OnBoardingWord.nikmatiKemudahanDenganBNIAgen46:
        return "Nikmati kemudahan dengan BNI Agen46";
      case OnBoardingWord.melayaniPalingDekat:
        return "#Melayani Paling Dekat";
      case OnBoardingWord.belumPunyaAkunBNIAgen46:
        return "Belum punya akun BNI Agen46 ?";
      case OnBoardingWord.daftarSekarang:
        return "\nDaftar Sekarang";
      case OnBoardingWord.masuk:
        return "Masuk";
      default:
        return "";
    }
  }
}

enum PreregisterWord {
  pendaftaranBNIAgen46,
  apakahAndaSudahMemilikiRekening,
  belumPunya,
  sudahPunya,
}

extension PreregisterText on PreregisterWord {
  String get text {
    switch (this) {
      case PreregisterWord.pendaftaranBNIAgen46:
        return "Pendaftaran BNI Agen46";
      case PreregisterWord.apakahAndaSudahMemilikiRekening:
        return "Apakah Anda sudah memiliki Rekening Taplus Bisnis BNI Agen46 ?";
      case PreregisterWord.belumPunya:
        return "Belum punya";
      case PreregisterWord.sudahPunya:
        return "Sudah punya";
      default:
        return "";
    }
  }
}

enum OpeningTncWord {
  syaratKetentuan,
  lanjut,
  sayaSetujuDengan,
  skRekeningTabungan,
  persetujuanPenawaranProduk,
}

extension OpeningTncText on OpeningTncWord {
  String get text {
    switch (this) {
      case OpeningTncWord.syaratKetentuan:
        return "Syarat & Ketentuan";
      case OpeningTncWord.lanjut:
        return "Lanjut";
      case OpeningTncWord.sayaSetujuDengan:
        return "Saya setuju dengan";
      case OpeningTncWord.skRekeningTabungan:
        return " “Syarat dan Ketentuan Rekening Tabungan Perorangan”";
      case OpeningTncWord.persetujuanPenawaranProduk:
        return " “Persetujuan Penawaran Produk dan Jasa Layanan”";
      default:
        return "";
    }
  }
}

enum InputPhoneNumberWord {
  inputNomorHandphone,
  lanjut,
  kamiAkanMelakukanProses,
  nomorAndaMemilikiPulsa,
  jagaKerahasiaanOTP,
  denganTidakMemberitahu,
  nomorHandphone,
  indonesia62,
  posisiAndaSaatIni,
  indonesia,
  singapore,
  thailand,
  brunei,
  india,
  china,
  vietnam,
  uSA,
  negara,
  pilihNegara,
  selesai,
  pastikanNomorHandphoneDialog,
}

extension InputPhoneNumberText on InputPhoneNumberWord {
  String get text {
    switch (this) {
      case InputPhoneNumberWord.inputNomorHandphone:
        return "Input Nomor Handphone";
      case InputPhoneNumberWord.lanjut:
        return "Lanjut";
      case InputPhoneNumberWord.kamiAkanMelakukanProses:
        return "Kami akan melakukan proses verifikasi nomor HP setelah proses pembukaan rekening selesai diproses. Pastikan ";
      case InputPhoneNumberWord.nomorAndaMemilikiPulsa:
        return "nomor Anda memiliki pulsa dan terkoneksi dengan jaringan Internet.";
      case InputPhoneNumberWord.jagaKerahasiaanOTP:
        return "Jaga kerahasiaan OTP ";
      case InputPhoneNumberWord.denganTidakMemberitahu:
        return "dengan tidak memberitahu kepada siapapun termasuk kepada Petugas Bank.";
      case InputPhoneNumberWord.nomorHandphone:
        return "Nomor Handphone";
      case InputPhoneNumberWord.indonesia62:
        return "Indonesia (+62)";
      case InputPhoneNumberWord.posisiAndaSaatIni:
        return "Posisi Anda Saat Ini";
      case InputPhoneNumberWord.indonesia:
        return "Indonesia";
      case InputPhoneNumberWord.singapore:
        return "Singapore";
      case InputPhoneNumberWord.thailand:
        return "Thailand";
      case InputPhoneNumberWord.brunei:
        return "Brunei";
      case InputPhoneNumberWord.india:
        return "India";
      case InputPhoneNumberWord.china:
        return "China";
      case InputPhoneNumberWord.vietnam:
        return "Vietnam";
      case InputPhoneNumberWord.uSA:
        return "USA";
      case InputPhoneNumberWord.negara:
        return "Negara";
      case InputPhoneNumberWord.pilihNegara:
        return "Pilih Negara";
      case InputPhoneNumberWord.pastikanNomorHandphoneDialog:
        return "Pastikan nomor Handphone yang Anda masukan sesuai format\ncontoh : 8123456789";
      case InputPhoneNumberWord.selesai:
        return "Selesai";
      default:
        return "";
    }
  }
}

enum PreTakeKtpWord {
  registrasi,
  lanjut,
  verifikasieKTP,
  prosesIniBertujuan,
  pastikanHasilFoto,
  sesuaiDenganIdentitasAnda
}

extension KtpRegistrationText on PreTakeKtpWord {
  String get text {
    switch (this) {
      case PreTakeKtpWord.registrasi:
        return "Registrasi";
      case PreTakeKtpWord.lanjut:
        return "Lanjut";
      case PreTakeKtpWord.verifikasieKTP:
        return "Verifikasi e-KTP";
      case PreTakeKtpWord.prosesIniBertujuan:
        return "Proses ini bertujuan untuk mempermudah\nmelakukan pengisian dan validasi data Anda.";
      case PreTakeKtpWord.pastikanHasilFoto:
        return "Pastikan hasil foto memenuhi ketentuan dibawah:";
      case PreTakeKtpWord.sesuaiDenganIdentitasAnda:
        return "• * sesuai dengan identitas Anda\n• * tidak silau & tidak buram\n• * terbaca jelas dan tidak terpotong\n• Tidak ada objek lain selain KTP dalam foto";
      default:
        return "";
    }
  }
}

enum OpeningAccountWord {
  bukaRekening,
  ayoBukaTabungan,
  jikaAndaSudahMempunyai,
  untukKelancaranPembukaan,
  eKtpWajib,
  lanjut,
}

extension OpeningAccountText on OpeningAccountWord {
  String get text {
    switch (this) {
      case OpeningAccountWord.bukaRekening:
        return "Buka Rekening";
      case OpeningAccountWord.ayoBukaTabungan:
        return "Ayo buka tabungan digital bisnis BNI Agen46 sekarang juga!";
      case OpeningAccountWord.jikaAndaSudahMempunyai:
        return "Jika Anda sudah mempunyai rekening tabungan bisnis BNI Agen46 , silakan lakukan pendaftaran pada\nhalaman sebelumnya dan pilih “Sudah Punya";
      case OpeningAccountWord.untukKelancaranPembukaan:
        return "Untuk kelancaran pembukaan tabungan, siapkan\ndahulu:";
      case OpeningAccountWord.eKtpWajib:
        return "• e-KTP (wajib) & NPWP (bila ada)\n• Pulsa / paket data untuk pengiriman OTP\n• Kertas & alat tulis untuk foto tanda tangan";
      case OpeningAccountWord.lanjut:
        return "Lanjut";
      default:
        return "";
    }
  }
}

enum TakeKtpWord {
  registrasi,
  pastikanPosisi,
  ktpAsli,
  pastikanFoto,
  sudahSesuai,
  fotoSudahSesuai,
  fotoUlang
}

extension TakeCameraKtpText on TakeKtpWord {
  String get text {
    switch (this) {
      case TakeKtpWord.registrasi:
        return "Registrasi";
      case TakeKtpWord.pastikanPosisi:
        return "Pastikan posisi & dan klik ambil foto. Pastikan foto terlihat dengan jelas.";
      case TakeKtpWord.ktpAsli:
        return "KTP asli pada area yang tersedia";
      case TakeKtpWord.pastikanFoto:
        return "Pastikan foto yang anda ambil ";
      case TakeKtpWord.sudahSesuai:
        return "sudah sesuai dan sudah fokus (tidak blur).";
      case TakeKtpWord.fotoSudahSesuai:
        return "Foto sudah sesuai";
      case TakeKtpWord.fotoUlang:
        return "Foto Ulang";
      default:
        return "";
    }
  }
}

enum AccountCardName {
  kartuDebitBNIAgen46GPNCombo1,
  kartuDebitBNIAgen46GPNCombo2,
  kartuDebitBNIAgen46GPNCombo3
}

extension AccountCardNameTitle on AccountCardName {
  String get title {
    switch (this) {
      case AccountCardName.kartuDebitBNIAgen46GPNCombo1:
        return "Kartu Debit BNI Agen46 GPN Combo";
      case AccountCardName.kartuDebitBNIAgen46GPNCombo2:
        return "Kartu Debit BNI Agen46 GPN Combo 2";
      case AccountCardName.kartuDebitBNIAgen46GPNCombo3:
        return "Kartu Debit BNI Agen46 GPN Combo 3";
      default:
        return "";
    }
  }
}

enum FeatureTitleList {
  biayaPengelolaan,
  setoranAwal,
  minSaldo,
  totalTarikTunai,
  limitTransaksiBelanja,
  transferAntarRekeningBNIviaATM,
  transferAntarBankviaATM,
}

extension FeatureTitle on FeatureTitleList {
  String get title {
    switch (this) {
      case FeatureTitleList.biayaPengelolaan:
        return 'Biaya Pengelolaan Rekening Per Bulan';
      case FeatureTitleList.setoranAwal:
        return 'Setoran Awal';
      case FeatureTitleList.minSaldo:
        return 'Min. Saldo Rata-Rata per Bulan  ';
      case FeatureTitleList.totalTarikTunai:
        return 'Limit Tarik Tunai via ATM ';
      case FeatureTitleList.limitTransaksiBelanja:
        return 'Limit Transaksi Belanja';
      case FeatureTitleList.transferAntarRekeningBNIviaATM:
        return 'Transfer Antar Rekening via ATM';
      case FeatureTitleList.transferAntarBankviaATM:
        return 'Transfer Antar Bank via ATM';
      default:
        return '';
    }
  }

  String get value1 {
    switch (this) {
      case FeatureTitleList.biayaPengelolaan:
        return 'Rp 0,- (6 bulan pertama) Rp 11.000,- per bulan (bulan selanjutnya)';
      case FeatureTitleList.setoranAwal:
        return 'Rp 50.000,-';
      case FeatureTitleList.minSaldo:
        return 'Rp 50.000,-';
      case FeatureTitleList.totalTarikTunai:
        return 'Rp 5 Juta/hari';
      case FeatureTitleList.limitTransaksiBelanja:
        return 'Rp 10 Juta/hari';
      case FeatureTitleList.transferAntarRekeningBNIviaATM:
        return 'Rp 50 Juta/hari';
      case FeatureTitleList.transferAntarBankviaATM:
        return 'Rp 10 Juta/hari';
      default:
        return '';
    }
  }

  String get value2 {
    switch (this) {
      case FeatureTitleList.biayaPengelolaan:
        return 'Rp 0,- (6 bulan pertama) Rp 11.000,- per bulan (bulan selanjutnya)';
      case FeatureTitleList.setoranAwal:
        return 'Rp 50.000,-';
      case FeatureTitleList.minSaldo:
        return 'Rp 50.000,-';
      case FeatureTitleList.totalTarikTunai:
        return 'Rp 15 Juta/hari';
      case FeatureTitleList.limitTransaksiBelanja:
        return 'Rp 100 Juta/hari';
      case FeatureTitleList.transferAntarRekeningBNIviaATM:
        return 'Rp 100 Juta/hari';
      case FeatureTitleList.transferAntarBankviaATM:
        return 'Rp 50 Juta/hari';
      default:
        return '';
    }
  }

  String get value3 {
    switch (this) {
      case FeatureTitleList.biayaPengelolaan:
        return 'Rp 0,- (6 bulan pertama) Rp 5.000,- per bulan (bulan selanjutnya)';
      case FeatureTitleList.setoranAwal:
        return 'Rp 50.000,-';
      case FeatureTitleList.minSaldo:
        return 'Rp 0,-';
      case FeatureTitleList.totalTarikTunai:
        return 'Rp 5 Juta/hari';
      case FeatureTitleList.limitTransaksiBelanja:
        return 'Rp 10 Juta/hari';
      case FeatureTitleList.transferAntarRekeningBNIviaATM:
        return 'Rp 50 Juta/hari';
      case FeatureTitleList.transferAntarBankviaATM:
        return 'Rp 10 Juta/hari';
      default:
        return '';
    }
  }

  String get value4 {
    switch (this) {
      case FeatureTitleList.biayaPengelolaan:
        return 'Rp 0,- (6 bulan pertama) Rp 10.000,- per bulan (bulan selanjutnya)';
      case FeatureTitleList.setoranAwal:
        return 'Rp 50.000,-';
      case FeatureTitleList.minSaldo:
        return 'Rp 50.000,-';
      case FeatureTitleList.totalTarikTunai:
        return 'Rp 15 Juta/hari';
      case FeatureTitleList.limitTransaksiBelanja:
        return 'Rp 50 Juta/hari';
      case FeatureTitleList.transferAntarRekeningBNIviaATM:
        return 'Rp 100 Juta/hari';
      case FeatureTitleList.transferAntarBankviaATM:
        return 'Rp 25 Juta/hari';
      default:
        return '';
    }
  }
}

enum FeatureExpandListDesc { desc1, desc2, desc3, desc4 }

enum FeatureExpandListDesc2 { desc1, desc2, desc3, desc4 }

enum FeatureExpandListDesc3 { desc1, desc2, desc3, desc4 }

extension FeatureExpandDesc on FeatureExpandListDesc {
  String get desc {
    switch (this) {
      case FeatureExpandListDesc.desc1:
        return "•  Nyaman transaksi dimana saja dan kapan saja dengan BNI Mobile Banking & Kartu Debit BNI\n\n•  Dapatkan berbagai promosi penawaranan menarik sepanjang tahun\n\n•  Kemudahan belanja online/e-commerce kapanpun dan dimanapun dengan teknologi 3D Secure\n\n•  Kemudahan Setor Tunai dan Tarik Tunai melalui ATM & CRM BNI pada ≥16.000 lokasi diseluruh Indonesia.";
      case FeatureExpandListDesc.desc2:
        return "Dapatkan seluruh kemudahan transaksi Tabungan BNI Taplus dengan ekstra benefit sebagai berikut :\n\n•  Cashback 5% per bulan (senilai Rp 100.000,-) dengan minimal transaksi Rp 1,5 juta untuk transaksi pembelian tiket pesawat di channel resmi milik Lion Group\n\n•   Cashback Rp 100.000,- untuk 20 orang top spender setiap bulan dengan minimal transaksi Rp 3 Juta di semua merchant.\n\n•   Spesial Line Check-In di bandara tertentu. \n\n•  Kartu Debit & Tapcash dalam 1 Kartu.";
      case FeatureExpandListDesc.desc3:
        return "•  Nyaman transaksi dimana saja dan kapan saja dengan BNI Mobile Banking & Kartu Debit BNI\n\n•  Dapatkan berbagai promosi penawaranan menarik sepanjang tahun\n\n•  Kemudahan belanja online/e-commerce kapanpun dan dimanapun dengan teknologi 3D Secure\n\n•  Kemudahan Setor Tunai dan Tarik Tunai melalui ATM & CRM BNI pada ≥16.000 lokasi diseluruh Indonesia.";
      case FeatureExpandListDesc.desc4:
        return "Dapatkan seluruh kemudahan transaksi tabungan BNI dengan ekstra benefit sebagai berikut :\n\n• Kemudahan menjadi merchant BNI dengan dukungan mesin EDC.\n\n•  Penawaran program khusus wirausaha & merchant sepanjang tahun.\n\n•  Laporan keuangan bulanan terinci. \n\n•  Kemudahan Kredit Usaha";
      default:
        return "";
    }
  }
}

extension FeatureExpandDesc2 on FeatureExpandListDesc2 {
  List<String> get title {
    switch (this) {
      case FeatureExpandListDesc2.desc1:
        return [
          "< Rp 1 Juta",
          "≥ Rp 1 Juta – Rp 50 Juta",
          "> Rp 50 Juta – Rp 500 Juta",
          "> Rp 500 Juta – Rp 1 Milyar",
          "> Rp 1 Milyar"
        ];
      case FeatureExpandListDesc2.desc2:
        return [
          "< Rp 1 Juta",
          "≥ Rp 1 Juta – Rp 50 Juta",
          "> Rp 50 Juta – Rp 500 Juta",
          "> Rp 500 Juta – Rp 1 Milyar",
          "> Rp 1 Milyar"
        ];
      case FeatureExpandListDesc2.desc3:
        return [
          "< Rp 1 Juta",
          "≥ Rp 1 Juta – Rp 10 Juta",
          "> Rp 10 Juta – Rp 50 Juta",
          "> Rp 50 Juta – Rp 100 Juta",
          "> Rp 100 Juta"
        ];
      case FeatureExpandListDesc2.desc4:
        return [
          "< Rp 5 Juta",
          "≥ Rp 5 Juta – Rp 100 Juta",
          "> Rp 100 Juta – Rp 1 Milyar",
          "> Rp 1 Milyar",
        ];
      default:
        return [];
    }
  }

  List<String> get value {
    switch (this) {
      case FeatureExpandListDesc2.desc1:
        return [
          "0.00%",
          "0.10%",
          "0.20%",
          "0.60%",
          "0.80%",
        ];
      case FeatureExpandListDesc2.desc2:
        return [
          "0.00%",
          "0.10%",
          "0.20%",
          "0.60%",
          "0.80%",
        ];
      case FeatureExpandListDesc2.desc3:
        return [
          "0.00%",
          "0.25%",
          "0.50%",
          "0.75%",
          "1%",
        ];
      case FeatureExpandListDesc2.desc4:
        return [
          "0.00%",
          "0.75%",
          "1%",
          "1.50%",
        ];
      default:
        return [];
    }
  }
}

extension FeatureExpandDesc3 on FeatureExpandListDesc3 {
  List<String> get title {
    return [
      "Biaya Penggantian Buku Tabungan",
      "Biaya Penggantian & Penerbitan Kartu Debit",
      "Biaya Penutupan Rekening",
      "Biaya Administrasi Kartu Debit Per Bulan",
      "Tunggakan Biaya Pengelolaan Rekening ",
      "Tunggakan Denda dibawah Saldo Minimum",
      "Tunggakan Biaya Administrasi Kartu Debit"
    ];
  }

  List<String> get value {
    switch (this) {
      case FeatureExpandListDesc3.desc1:
        return [
          "Rp 1.500,-",
          "Maks. Rp 15.000,-",
          "Rp 10.000,-",
          "Rp 4.000,-",
          "Maks. 3x tunggakan",
          "Maks. 3x tunggakan",
          "Rp 10.000,-",
        ];
      case FeatureExpandListDesc3.desc2:
        return [
          "Rp 1.500,-",
          "Rp 25.000,-",
          "Rp 10.000,-",
          "Rp 10.000,-",
          "Maks. 3x tunggakan",
          "Maks. 3x tunggakan",
          "Rp 10.000,-",
        ];
      case FeatureExpandListDesc3.desc3:
        return [
          "Rp 1.500,-",
          "Maks. Rp 15.000,-",
          "Rp 50.000,-",
          "Rp 4.000,-",
          "Maks. 3x tunggakan",
          "Maks. 3x tunggakan",
          "Rp 10.000,-",
        ];
      case FeatureExpandListDesc3.desc4:
        return [
          "Rp 0,-",
          "Maks. Rp 20.000,-",
          "Rp 25.000,-",
          "Rp 7.500,-",
          "Maks. 3x tunggakan",
          "Maks. 3x tunggakan",
          "Rp 10.000,-",
        ];
      default:
        return [];
    }
  }
}

enum RegistrationFormPrivateLabel {
  namaLengkap,
  tempatLahir,
  tanggalLahir,
  jenisKelamin,
  alamat,
  rt,
  rw,
  provinsi,
  kotaKabupaten,
  kecamatan,
  desaKelurahan,
  agama,
  kodePos,
  kotaPenerbitIndentitas,
  email,
  nomorTelepon,
  nomorTeleponRumah,
  nomorNpwp,
  statusPerkawinan,
  namaIbuKandung,
}

extension FormPrivateTitle on RegistrationFormPrivateLabel {
  String get title {
    switch (this) {
      case RegistrationFormPrivateLabel.namaLengkap:
        return "Nama Lengkap";
      case RegistrationFormPrivateLabel.tempatLahir:
        return "Tempat Lahir";
      case RegistrationFormPrivateLabel.tanggalLahir:
        return "Tanggal Lahir";
      case RegistrationFormPrivateLabel.jenisKelamin:
        return "Jenis Kelamin";
      case RegistrationFormPrivateLabel.alamat:
        return "Alamat";
      case RegistrationFormPrivateLabel.rt:
        return "RT";
      case RegistrationFormPrivateLabel.rw:
        return "RW";
      case RegistrationFormPrivateLabel.provinsi:
        return "Provinsi";
      case RegistrationFormPrivateLabel.kotaKabupaten:
        return "Kota / Kabupaten";
      case RegistrationFormPrivateLabel.kecamatan:
        return "Kecamatan";
      case RegistrationFormPrivateLabel.desaKelurahan:
        return "Desa / Kelurahan";
      case RegistrationFormPrivateLabel.agama:
        return "Agama";
      case RegistrationFormPrivateLabel.kodePos:
        return "Kode Pos";
      case RegistrationFormPrivateLabel.kotaPenerbitIndentitas:
        return "Kota Penerbit Identitas";
      case RegistrationFormPrivateLabel.email:
        return "Email";
      case RegistrationFormPrivateLabel.nomorTelepon:
        return "Nomor Telepon";
      case RegistrationFormPrivateLabel.nomorTeleponRumah:
        return "Nomor Telepon Rumah";
      case RegistrationFormPrivateLabel.nomorNpwp:
        return "Nomor NPWP";
      case RegistrationFormPrivateLabel.statusPerkawinan:
        return "Status Perkawinan";
      case RegistrationFormPrivateLabel.namaIbuKandung:
        return "Nama Ibu Kandung";
      default:
        return "";
    }
  }
}

enum RegistrationFormJobDetailLabel {
  pekerjaan,
  penghasilanPerbulan,
  sumberDana,
  perkiraanNilaiTransaksi,
  tujuanPembukaanRekening,
  jabatan,
  namaTempatKerjaPerusahaan,
  noTeleponTempatBekerja,
  alamatTempatKerja,
  kodePos,
}

extension FormJobTitle on RegistrationFormJobDetailLabel {
  String get title {
    switch (this) {
      case RegistrationFormJobDetailLabel.pekerjaan:
        return "Pekerjaan";
      case RegistrationFormJobDetailLabel.penghasilanPerbulan:
        return "Penghasilan Perbulan";
      case RegistrationFormJobDetailLabel.sumberDana:
        return "Sumber Dana";
      case RegistrationFormJobDetailLabel.perkiraanNilaiTransaksi:
        return "Perkiraan Nilai Transaksi";
      case RegistrationFormJobDetailLabel.tujuanPembukaanRekening:
        return "Tujuan Pembukaan Rekening";
      case RegistrationFormJobDetailLabel.jabatan:
        return "Jabatan";
      case RegistrationFormJobDetailLabel.namaTempatKerjaPerusahaan:
        return "Nama Tempat Kerja Perusahaan";
      case RegistrationFormJobDetailLabel.noTeleponTempatBekerja:
        return "No.Telepon Tempat Bekerja";
      case RegistrationFormJobDetailLabel.alamatTempatKerja:
        return "Alamat Tempat Kerja";
      case RegistrationFormJobDetailLabel.kodePos:
        return "Kode Pos";
      default:
        return "";
    }
  }
}

enum RegistrationFormOfficeBranchLabel {
  provinsi,
  kotaKabupaten,
  kantorCabang,
  alamatKantorCabang
}

extension FormOfficeBranchTitle on RegistrationFormOfficeBranchLabel {
  String get title {
    switch (this) {
      case RegistrationFormOfficeBranchLabel.provinsi:
        return "Provinsi";
      case RegistrationFormOfficeBranchLabel.kotaKabupaten:
        return "Kota / Kabupaten";
      case RegistrationFormOfficeBranchLabel.kantorCabang:
        return "Kantor Cabang";
      case RegistrationFormOfficeBranchLabel.alamatKantorCabang:
        return "Alamat Kantor Cabang";
      default:
        return "";
    }
  }
}

enum TakeSelfieAndKtpWord {
  fotoSelfieDenganKtp,
  pastikan,
  wajahDanKtp,
  danAmbilFoto,
  pastikanFoto,
  sudahSesuai,
  fotoSudahSesuai,
  fotoUlang
}

extension TakeCameraSelfieAndKtpText on TakeSelfieAndKtpWord {
  String get text {
    switch (this) {
      case TakeSelfieAndKtpWord.fotoSelfieDenganKtp:
        return "Foto Selfie dengan KTP";
      case TakeSelfieAndKtpWord.pastikan:
        return "Pastikan ";
      case TakeSelfieAndKtpWord.wajahDanKtp:
        return "wajah dan KTP asli Anda memenuhi area";
      case TakeSelfieAndKtpWord.danAmbilFoto:
        return "dan ambil foto ketika Anda siap.";
      case TakeSelfieAndKtpWord.pastikanFoto:
        return "Pastikan foto yang anda ambil ";
      case TakeSelfieAndKtpWord.sudahSesuai:
        return "sudah sesuai dan sudah fokus (tidak blur).";
      case TakeSelfieAndKtpWord.fotoSudahSesuai:
        return "Foto sudah sesuai";
      case TakeSelfieAndKtpWord.fotoUlang:
        return "Foto Ulang";
      default:
        return "";
    }
  }
}
