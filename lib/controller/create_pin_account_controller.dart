// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'package:doasdk/controller/main_controller.dart';
import 'package:doasdk/doasdk.dart';

class CreatePinAccountController extends GetxController {
  final MainController _mController = Get.find();
  final RxString _pinTextController = RxString('');
  final RxString _rePinTextController = RxString('');
  final RxBool _pinVisibility = RxBool(true);
  final RxBool _rePinVisibility = RxBool(true);

  Color borderPinColor(bool isPin, int i) {
    return pinController(isPin).value.length == (i + 1)
        ? ORANGE
        : GREY_BACKGROUND;
  }

  RxString pinController(bool isPin) {
    if (isPin) {
      return _pinVisibility.value
          ? RxString(
              List.generate(_pinTextController.value.length, (i) => "*").join())
          : _pinTextController;
    } else {
      return _rePinVisibility.value
          ? RxString(
              List.generate(_rePinTextController.value.length, (i) => "*")
                  .join())
          : _rePinTextController;
    }
  }

  void Function() changeVisibility(bool isPin) {
    if (isPin) {
      return changePinVisibility();
    } else {
      return changeRePinVisibility();
    }
  }

  void Function() changePinVisibility() {
    return () {
      _pinVisibility.value = !_pinVisibility.value;
    };
  }

  void Function() changeRePinVisibility() {
    return () {
      _rePinVisibility.value = !_rePinVisibility.value;
    };
  }

  RxBool pinVisibility(bool isPin) {
    if (isPin) {
      return _pinVisibility;
    } else {
      return _rePinVisibility;
    }
  }

  void Function(String)? seletedNum() {
    return (n) {
      var _pin = n.split("");
      if (_pin.length <= 6) {
        _pinTextController.value = _pin.getRange(0, _pin.length).join();
      }
      if (_pin.length >= 6) {
        _rePinTextController.value = _pin.getRange(6, _pin.length).join();
      }
    };
  }

  void Function()? next() {
    if (_rePinTextController.value.length != 6) {
      return null;
    }
    return () async {
      var _validation = _rePinTextController.value == _pinTextController.value;
      if (!_validation) {
        DIALOG_HELPER("Pin yg Anda masukan tidak sama");
        return;
      }
      await Get.toNamed(ROUTE.processingData.name);
    };
  }

  @override
  void onReady() {
    _mController.startProgressAnim();
    super.onReady();
  }
}
