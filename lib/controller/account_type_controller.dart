// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:carousel_slider/carousel_options.dart';
import 'package:doasdk/controller/main_controller.dart';
import 'package:doasdk/doasdk.dart';

class AccountTypeController extends GetxController
    with GetSingleTickerProviderStateMixin {
  final MainController _mController = Get.find();
  late AnimationController expandController;
  late Rx<Animation<double>> animation;
  List<String> accountAsset = [
    debitTaplusMudaAssets,
    debitBatikAirAssets,
    debitSilverAssets,
    debitGoldAssets,
  ];
  RxBool isShowFullFeature = false.obs;
  RxString showFullFeatureBtnTitle = "Selengkapnya".obs;
  RxString expandDesc = FeatureExpandListDesc.desc1.desc.obs;
  RxList<String> expandDesc2 = FeatureExpandListDesc2.desc1.title.obs;
  RxList<String> expandDesc2Value = FeatureExpandListDesc2.desc1.value.obs;
  List<String> expandDesc3 = FeatureExpandListDesc3.desc1.title;
  RxList<String> expandDesc3Value = FeatureExpandListDesc3.desc1.value.obs;
  RxDouble showFullFeatureArrowAngle = 15.0.obs;
  RxList<String> featureValue =
      FeatureTitleList.values.map((e) => e.value1).toList().obs;
  RxString accountCardName = "Kartu Debit Taplus Silver Mastercard".obs;
  RxString accountCardHeader =
      "Semakin mudah dan nyaman bertransaksi dengan setoran awal yang ringan dan bebas biaya administrasi selama 6 bulan pertama."
          .obs;
  ScrollController scController = ScrollController();

  int get featureLength {
    if (isShowFullFeature.value) {
      return FeatureTitleList.values.length;
    } else {
      return FeatureTitleList.values.take(3).length;
    }
  }

  RxInt corouselIndex = 0.obs;

  @override
  void onInit() {
    _prepareAnimations();
    super.onInit();
  }

  @override
  void onReady() {
    _mController.startProgressAnim();
    scrollToDown();
    super.onReady();
  }

  Function(int index, CarouselPageChangedReason reason)? onPageChanged() {
    return (index, reason) {
      corouselIndex.value = index;
      switch (index) {
        case 0:
          featureValue.value =
              FeatureTitleList.values.map((e) => e.value1).toList();
          accountCardHeader.value =
              "Semakin mudah dan nyaman bertransaksi dengan setoran awal yang ringan dan bebas biaya administrasi selama 6 bulan pertama.";
          expandDesc.value = FeatureExpandListDesc.desc1.desc;
          expandDesc2.value = FeatureExpandListDesc2.desc1.title;
          expandDesc2Value.value = FeatureExpandListDesc2.desc1.value;
          expandDesc3Value.value = FeatureExpandListDesc3.desc1.value;
          accountCardName.value = "Kartu Debit Taplus Silver Mastercard";
          break;
        case 1:
          featureValue.value =
              FeatureTitleList.values.map((e) => e.value2).toList();
          accountCardHeader.value =
              "Nikmati seluruh layanan utama Tabungan BNI Taplus dengan ekstra berbagai benefit dari Kartu Debit BNI Co-Brand Batik Air ";
          expandDesc.value = FeatureExpandListDesc.desc2.desc;
          expandDesc2.value = FeatureExpandListDesc2.desc2.title;
          expandDesc2Value.value = FeatureExpandListDesc2.desc2.value;
          expandDesc3Value.value = FeatureExpandListDesc3.desc2.value;
          accountCardName.value = "Kartu Debit BNI Co-Brand Batik Air";
          break;
        case 2:
          featureValue.value =
              FeatureTitleList.values.map((e) => e.value3).toList();
          accountCardHeader.value =
              "Gabung dengan BNI Taplus Muda untuk kamu yang memiliki jiwa muda dan buat impian kamu jadi nyata #LetsMakeItReal";
          expandDesc.value = FeatureExpandListDesc.desc3.desc;
          expandDesc2.value = FeatureExpandListDesc2.desc3.title;
          expandDesc2Value.value = FeatureExpandListDesc2.desc3.value;
          expandDesc3Value.value = FeatureExpandListDesc3.desc3.value;
          accountCardName.value = "Kartu Debit Virtual Silver Mastercard";
          break;
        case 3:
          featureValue.value =
              FeatureTitleList.values.map((e) => e.value4).toList();
          accountCardHeader.value =
              "Mendukung setiap transaksi keuangan dan bisnis usaha anda";
          expandDesc.value = FeatureExpandListDesc.desc4.desc;
          expandDesc2.value = FeatureExpandListDesc2.desc4.title;
          expandDesc2Value.value = FeatureExpandListDesc2.desc4.value;
          expandDesc3Value.value = FeatureExpandListDesc3.desc4.value;
          accountCardName.value = "Kartu Debit Gold Master Card";
          break;
        default:
      }
    };
  }

  Color carouselIndexColor(int i) {
    return corouselIndex.value == i ? ORANGE : GREY;
  }

  void Function() showFullFeature() {
    return () async {
      isShowFullFeature.value = !isShowFullFeature.value;
      if (isShowFullFeature.value) {
        showFullFeatureArrowAngle.value = 5.0;
        showFullFeatureBtnTitle.value = "Sembunyikan";
        expandController.forward();
        scrollToDown();
      } else {
        showFullFeatureBtnTitle.value = "Selengkapnya";
        showFullFeatureArrowAngle.value = 15.0;
        expandController.reverse();
      }
    };
  }

  void _prepareAnimations() {
    expandController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 500));
    animation = CurvedAnimation(
      parent: expandController,
      curve: Curves.fastOutSlowIn,
    ).obs;
  }

  void Function()? next() {
    return () async {
      try {
        var data = readUserData()?.toJson();
        data?.update("card_type", (value) => accountCardName.value,
            ifAbsent: () => accountCardName.value);
        await writeUserData(data!);
        await Get.toNamed(ROUTE.preTakeKtp.name);
      } catch (e) {
        DIALOG_HELPER("$e");
        return;
      }
    };
  }

  Future<void> scrollToDown() async {
    await Future.delayed(const Duration(milliseconds: 100));
    await scController.animateTo(
      Get.height,
      duration: const Duration(seconds: 2),
      curve: Curves.fastOutSlowIn,
    );
  }
}
