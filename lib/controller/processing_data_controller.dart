// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:doasdk/doasdk.dart';

class ProcessingDataController extends GetxController
    with GetTickerProviderStateMixin {
  late AnimationController _animationController;
  RxDouble progressAnimation = RxDouble(0);

  @override
  void onInit() {
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 5),
    )..addListener(() {
        progressAnimation.value = _animationController.value;
      });
    _animationController.repeat();
    super.onInit();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  void onReady() async {
    await Future.delayed(const Duration(seconds: 4));
    _animationController.dispose();
    await Get.offAllNamed(ROUTE.completeRegistration.name);
    super.onReady();
  }
}
