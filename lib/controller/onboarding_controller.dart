// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:carousel_slider/carousel_slider.dart';
import 'package:doasdk/doasdk.dart';

class OnBoardingController extends GetxController {
  RxInt corouselIndex = 0.obs;
  Color carouselIndexColor(int i) {
    return corouselIndex.value == i ? BLUE_DARK : GREY;
  }

  Function(int index, CarouselPageChangedReason reason)? onPageChanged() {
    return (index, reason) {
      corouselIndex.value = index;
    };
  }

  void Function()? next() {
    return () async {
      // return await Get.toNamed(ROUTE.preRegister.name);
    };
  }
}
