// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:doasdk/controller/main_controller.dart';
import 'package:doasdk/doasdk.dart';

class PreTakeSignatureController extends GetxController {
  final MainController _mController = Get.find();

  void Function()? next() {
    return () async {
      await Get.toNamed(ROUTE.takeSignature.name);
    };
  }

  @override
  void onReady() {
    _mController.startProgressAnim();
    super.onReady();
  }
}
