// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// ignore_for_file: unused_local_variable, no_leading_underscores_for_local_identifiers

import 'dart:math';
import 'package:doasdk/controller/main_controller.dart';
import 'package:doasdk/doasdk.dart';

class DataVerificationController extends GetxController {
  final MainController _mController = Get.find();

  Widget helperDescriptionIcon(int i) {
    switch (i) {
      case 0:
        return ImageIcon(AssetImage(lampAssets), color: BLUE_TEXT);
      case 1:
        return ImageIcon(AssetImage(whatsappAssets), color: BLUE_TEXT);
      case 2:
        return ImageIcon(AssetImage(keyAssets), color: BLUE_TEXT);
      default:
        return Container();
    }
  }

  Widget helperDescription(int i) {
    switch (i) {
      case 0:
        return Text.rich(
          TextSpan(text: "Pastikan Anda ", children: [
            TextSpan(
                text: "memiliki kuota SMS pascabayar / kuota SMS",
                style: textStyleW600(fontColor: BLUE_TEXT))
          ]),
          style: textStyleW500(fontSize: 12, fontColor: BLUE_TEXT),
        );
      case 1:
        return Text.rich(
          TextSpan(
              text: "Untuk metode ",
              style: textStyleW500(fontSize: 12, fontColor: BLUE_TEXT),
              children: [
                TextSpan(
                    text: "Whatsapp, ",
                    style: textStyleW600(fontColor: BLUE_TEXT)),
                const TextSpan(text: "pastikan Anda "),
                TextSpan(
                    text: "terhubung ke jaringan internet.",
                    style: textStyleW600(fontColor: BLUE_TEXT))
              ]),
          style: textStyleW500(fontSize: 12, fontColor: BLUE_TEXT),
        );
      case 2:
        return Text.rich(
            TextSpan(text: "Jaga kerahasiaan OTP .", children: [
              TextSpan(
                  text:
                      "dengan tidak memberitahu kepada siapapun termasuk kepada Petugas Bank",
                  style: textStyleW600(fontColor: BLUE_TEXT))
            ]),
            style: textStyleW500(fontSize: 12, fontColor: BLUE_TEXT));
      default:
        return Container();
    }
  }

  void Function()? sentSmsVerificationCode() {
    return () async {
      try {
        if (_mController.time.value != "0") {
          DIALOG_HELPER("", costumText: Obx(() {
            return Text.rich(
              TextSpan(children: [
                const TextSpan(text: "Perhatian Mohon tunggu\n"),
                if (_mController.time.value != "0")
                  TextSpan(
                      text: _mController.time.value,
                      style: textStyleW700(fontSize: 16, fontColor: ORANGE)),
                const TextSpan(text: "\nuntuk mendapat kode verifikasi"),
              ]),
              style: textStyleW600(),
              textAlign: TextAlign.center,
            );
          }));
          return;
        }
        if (readUserData()?.number == null) {
          return;
        }
        if (_mController.hasRequestNotificationPermission) {
          Get.toNamed(ROUTE.inputOtpCode.name);
          await Future.delayed(const Duration(seconds: 2));
          await _mController.sendSmsNotification();
        } else {
          await DIALOG_HELPER("Mohon Izinkan Notifikasi permisi");
        }
      } catch (e) {
        return;
      }
    };
  }

  void Function()? sentWhatsappVerificationCode() {
    return () async {
      try {
        if (_mController.time.value != "0") {
          DIALOG_HELPER("", costumText: Obx(() {
            return Text.rich(
              TextSpan(children: [
                const TextSpan(text: "Perhatian Mohon tunggu\n"),
                if (_mController.time.value != "0")
                  TextSpan(
                      text: _mController.time.value,
                      style: textStyleW700(fontSize: 16, fontColor: ORANGE)),
                const TextSpan(text: "\nuntuk mendapat kode verifikasi"),
              ]),
              style: textStyleW600(),
              textAlign: TextAlign.center,
            );
          }));
          return;
        }
        if (readUserData()?.number == null) {
          return;
        }
        var _phoneNumber = "6285163054352";
        var _rnd = Random();
        var _l = List.generate(5, (_) => _rnd.nextInt(9));
        var _text = _l.join("");
        Get.toNamed(ROUTE.inputOtpCode.name);
        await _mController.sendWhatsappMethodChannel();
      } catch (e) {
        return;
      }
    };
  }

  @override
  void onReady() {
    _mController.startProgressAnim();
    super.onReady();
  }
}
