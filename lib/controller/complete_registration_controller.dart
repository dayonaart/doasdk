// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:doasdk/controller/main_controller.dart';
import 'package:doasdk/doasdk.dart';
import 'package:doasdk/model/complete_registration_model.dart';
import 'package:flutter/services.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:screenshot/screenshot.dart';

class CompleteRegistrationController extends GetxController {
  final MainController _mController = Get.find();
  final callbackData = CompleteRegistrationModel(
      accountNumber: randomNumber(10),
      virtualNumber: randomAccountNumber,
      customersName: "${readUserData()?.fullName}",
      cardType: "${readUserData()?.cardType}",
      createdDate: DateFormat("dd MMMM yyyy").format(DateTime.now()),
      createdTime:
          "${DateFormat.Hms().format(DateTime.now()).replaceAll(".", ":")} WIB",
      officeBranch: "${readUserData()?.officeBranch}",
      cardDetail: "Terbuka");

  ScreenshotController screenshotController = ScreenshotController();
  void Function() next() {
    return () async {
      try {
        onCompleteCallback!(200, callbackData);
        Navigator.pop(clientContext!);
      } catch (e) {
        DIALOG_HELPER("$e");
      }
    };
  }

  void Function() saveCompleteRegistration() {
    return () async {
      var _save = await screenshotController.capture();
      await ImageGallerySaver.saveImage(_save!,
          quality: 100, name: "doa_registration");
      Get.rawSnackbar(
          snackPosition: SnackPosition.TOP,
          title: "Perhatian",
          message: "Berhasil disimpan!",
          snackStyle: SnackStyle.FLOATING);
    };
  }

  void Function() copyAccountNumber() {
    return () async {
      try {
        await Clipboard.setData(
            ClipboardData(text: callbackData.accountNumber));
        Get.snackbar("Perhatian", "Berhasil di Copy!");
      } catch (e) {
        Get.snackbar("Perhatian", "$e");
      }
    };
  }

  @override
  void onReady() {
    completeRegistrationModel = callbackData;
    _mController.startProgressAnim();
    super.onReady();
  }
}
