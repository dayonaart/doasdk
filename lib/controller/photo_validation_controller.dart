// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:doasdk/controller/main_controller.dart';
import 'package:doasdk/doasdk.dart';

class PhotoValidationController extends GetxController {
  final MainController _mController = Get.find();

  void Function()? next() {
    return () async {
      await Get.toNamed(ROUTE.dataVerification.name);
    };
  }

  void Function()? changeData(String? title) {
    switch (title) {
      case "Foto Selfie dengan KTP":
        return () async {
          _mController.setRetakeFile(true);
          await Get.toNamed(ROUTE.selfieAndKtp.name);
        };
      case "Tanda Tangan":
        return () async {
          _mController.setRetakeFile(true);
          await Get.toNamed(ROUTE.takeSignature.name);
        };
      case "NPWP (Opsional)":
        return () async {
          _mController.setRetakeFile(true);
          await Get.toNamed(ROUTE.takeNpwp.name);
        };
      default:
        return null;
    }
  }

  @override
  void onReady() async {
    _mController.startProgressAnim();
    super.onReady();
  }
}
