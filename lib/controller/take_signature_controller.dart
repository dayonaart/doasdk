// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:doasdk/controller/main_controller.dart';
import 'package:doasdk/doasdk.dart';

class TakeSignatureController extends GetxController {
  final MainController _mController = Get.find();

  void Function()? onAccept() {
    return () async {
      Get.close(3);
      await Get.toNamed(ROUTE.preTakeNpwp.name);
    };
  }

  void Function()? onReject() {
    return () {
      Get.close(2);
    };
  }

  void Function()? onCompleteCamera() {
    return () async {
      await Get.toNamed(ROUTE.previewTakeImage.name);
    };
  }

  @override
  void onReady() {
    _mController.startProgressAnim();
    super.onReady();
  }
}
