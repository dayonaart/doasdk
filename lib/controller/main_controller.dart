// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:camera/camera.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:imager/image.dart' as img;
import 'package:doasdk/doasdk.dart';

class MainController extends GetxController
    with GetSingleTickerProviderStateMixin, WidgetsBindingObserver {
  final MethodChannel _doasdk = const MethodChannel("doasdk");

  late List<CameraDescription> cameras;
  late CameraController? camController;
  bool isCameraReady = false;
  late AnimationController progresscontroller;
  RxDouble progress = 0.0.obs;
  Rx<File?> ktpFile = Rx(null);
  Rx<File?> selfieFile = Rx(null);
  Rx<File?> signatureFile = Rx(null);
  Rx<File?> npwpFile = Rx(null);
  Rx<File?> retakeFile = Rx(null);
  bool _isCameraBusy = false;
  bool _retakeFile = false;
  bool hasRequestNotificationPermission = true;
  // ignore: unused_field
  Timer? _timer;
  final time = '0'.obs;

  Rx<String?> secretOtpCode = Rx(null);
  RxBool isWhatsappOtp = RxBool(false);
  void setRetakeFile(bool retake) {
    _retakeFile = retake;
  }

  Future<void> _setImagePath(String fileName, File file) async {
    if (fileName == "KTP") {
      ktpFile.value = file;
    } else if (fileName == "SELFIE_KTP") {
      selfieFile.value = file;
    } else if (fileName == "SIGNATURE") {
      signatureFile.value = file;
    } else if (fileName == "NPWP") {
      npwpFile.value = file;
    } else if (fileName == "RETAKE") {
      retakeFile.value = file;
    } else {
      return;
    }
  }

  Future<void> initCameraController({bool front = false}) async {
    var _camDesc = cameras.firstWhere((description) =>
        description.lensDirection ==
        (front ? CameraLensDirection.front : CameraLensDirection.back));
    camController = CameraController(_camDesc, ResolutionPreset.high);

    await camController?.initialize().then((_) async {
      isCameraReady = (camController != null);
      if (isCameraReady) {
        camController!.setFlashMode(FlashMode.off);
      }
      update();
    }).catchError((Object e) {
      if (e is CameraException) {
        switch (e.code) {
          case 'CameraAccessDenied':
            DIALOG_HELPER("CameraAccessDenied");
            break;
          default:
            DIALOG_HELPER("errors $e");
            break;
        }
      }
    });
  }

  void Function() changeCameraDirection() {
    return () async {
      final lensDirection = camController?.description.lensDirection;
      if (lensDirection == CameraLensDirection.front) {
        await initCameraController(front: false);
      } else {
        await initCameraController(front: true);
      }
    };
  }

  /// file name
  ///
  /// 1.KTP
  ///
  /// 2.SELFIE_KTP
  ///
  /// 3.SIGNATURE
  ///
  /// 4.NPWP
  void Function() takePicture({
    @required String? btnAccTitle,
    @required String? btnRejectTitle,
    @required void Function()? onAccept,
    @required void Function()? onReject,
    @required void Function()? onCompleteCamera,
    @required Widget? bottomDialogChild,
    @required String? fileName,
  }) {
    assert(fileName != null);
    return () async {
      try {
        if (_isCameraBusy) {
          return;
        }
        _isCameraBusy = true;
        if (_retakeFile) {
          var _xFile = await camController?.takePicture();
          if (_xFile == null) {
            _retakeFile = false;
            _isCameraBusy = false;
            retakeFile.value = null;
            return;
          }
          await _setImagePath("RETAKE", File(_xFile.path));
        } else {
          var _xFile = await camController?.takePicture();
          if (_xFile == null) {
            _retakeFile = false;
            _isCameraBusy = false;
            return;
          }
          await _setImagePath(fileName!, File(_xFile.path));
        }
        onCompleteCamera?.call();
        await Future.delayed(const Duration(milliseconds: 500));
        await BOTTOM_DIALOG_CONFIRMATION(
            btnAccTitle: btnAccTitle,
            btnRejectTitle: btnRejectTitle,
            topTitle: bottomDialogChild,
            onAccept: () async {
              _isCameraBusy = false;
              await camController?.dispose();
              if (_retakeFile) {
                _retakeFile = false;
                await _setImagePath(fileName!, File(retakeFile.value!.path));
                retakeFile.value = null;
                Get.close(3);
                return;
              }
              onAccept?.call();
            },
            onReject: () {
              _isCameraBusy = false;
              if (_retakeFile) {
                retakeFile.value = null;
                Get.close(2);
                return;
              }
              onReject?.call();
            });
      } catch (e) {
        _retakeFile = false;
        _isCameraBusy = false;
        retakeFile.value = null;
        DIALOG_HELPER("$e");
      }
    };
  }

  Future<Uint8List?> cropImage(
    File? file, {
    bool isKtp = false,
    bool useOringalImage = false,
  }) async {
    try {
      var _bytes = await file!.readAsBytes();
      if (useOringalImage) {
        return _bytes;
      }
      img.Image? _src = img.decodeImage(_bytes);
      var cropSize = min(_src!.width, _src.height);
      int offsetX = (_src.width - min(_src.width, _src.height));
      int offsetY = (_src.height - min(_src.width, _src.height));
      var _img = img.copyCrop(
        _src,
        offsetX,
        offsetY ~/ 9,
        cropSize,
        cropSize,
      );
      // var _fixOrientation = img.copyRotate(_img, 90);
      return Uint8List.fromList(img.encodeJpg(_img));
    } catch (e) {
      return null;
    }
  }

  void startProgressAnim() {
    if (progresscontroller.status == AnimationStatus.completed) {
      progresscontroller.reset();
      progresscontroller.forward();
      return;
    }
    progresscontroller.forward();
  }

  void disposeAnime() {
    progresscontroller.dispose();
  }

  Future<void> checkPermission(
    Permission permission, {
    String optionalPermissionName = '',
  }) async {
    String _permissionName =
        (permission.toString().split(".")[1] + optionalPermissionName)
            .toUpperCase();
    var _checkPermission = await permission.request();
    if (await permission.isGranted) {
      return;
    }
    if (_checkPermission.isPermanentlyDenied) {
      await DIALOG_HELPER_PERMISSION(
          "Perhatian Akses $_permissionName Diperlukan");
      if (!(await openAppSettings())) {
        await DIALOG_HELPER_PERMISSION(
            "Terjadi kesalahan\nTidak dapat membuka pengaturan");
        await Future.delayed(const Duration(milliseconds: 500));
        // ignore: use_build_context_synchronously
        Get.back();
      }
      await Future.delayed(const Duration(milliseconds: 500));
      // ignore: use_build_context_synchronously
      Get.back();
    }
    if (_checkPermission.isDenied) {
      await DIALOG_HELPER_PERMISSION(
          "Perhatian Akses $_permissionName Diperlukan");
      await checkPermission(permission);
    }
  }

  @override
  void onInit() async {
    WidgetsFlutterBinding.ensureInitialized();
    progresscontroller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1),
    )..addListener(() {
        progress.value = progresscontroller.value;
      });
    super.onInit();
  }

  @override
  void onReady() async {
    cameras = await availableCameras();
    super.onReady();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    final CameraController? cameraController = camController;
    if (cameraController == null || !cameraController.value.isInitialized) {
      return;
    }

    if (state == AppLifecycleState.inactive) {
      cameraController.dispose();
    } else if (state == AppLifecycleState.resumed) {
      onNewCameraSelected(cameraController.description);
    }
    super.didChangeAppLifecycleState(state);
  }

  Future<void> onNewCameraSelected(CameraDescription cameraDescription) async {
    final CameraController? oldController = camController;
    if (oldController != null) {
      camController = null;
      await oldController.dispose();
    }

    final CameraController cameraController = CameraController(
      cameraDescription,
      ResolutionPreset.low,
      enableAudio: true,
      imageFormatGroup: ImageFormatGroup.jpeg,
    );

    camController = cameraController;
    cameraController.addListener(() {
      if (cameraController.value.hasError) {
        DIALOG_HELPER(
            'Camera error ${cameraController.value.errorDescription}');
      }
    });

    try {
      await cameraController.initialize();
    } on CameraException catch (e) {
      switch (e.code) {
        case 'CameraAccessDenied':
          DIALOG_HELPER_PERMISSION('You have denied camera access.');
          break;
        case 'CameraAccessDeniedWithoutPrompt':
          // iOS only
          DIALOG_HELPER_PERMISSION(
              'Please go to Settings app to enable camera access.');
          break;
        case 'CameraAccessRestricted':
          // iOS only
          DIALOG_HELPER_PERMISSION('Camera access is restricted.');
          break;
        case 'AudioAccessDenied':
          DIALOG_HELPER_PERMISSION('You have denied audio access.');
          break;
        case 'AudioAccessDeniedWithoutPrompt':
          // iOS only
          DIALOG_HELPER_PERMISSION(
              'Please go to Settings app to enable audio access.');
          break;
        case 'AudioAccessRestricted':
          // iOS only
          DIALOG_HELPER_PERMISSION('Audio access is restricted.');
          break;
        default:
          DIALOG_HELPER_PERMISSION('CAMERA EXCEPTION $e');

          break;
      }
    }
  }

  // FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  //     FlutterLocalNotificationsPlugin();
  // Future<void> notificationPermission() async {
  //   const AndroidInitializationSettings initializationSettingsAndroid =
  //       AndroidInitializationSettings('launch_background');
  //   const InitializationSettings initializationSettings =
  //       InitializationSettings(
  //     android: initializationSettingsAndroid,
  //   );
  //   flutterLocalNotificationsPlugin.initialize(initializationSettings,
  //       onDidReceiveNotificationResponse: didReceiveLocalNotification());

  //   hasRequestNotificationPermission = await flutterLocalNotificationsPlugin
  //           .resolvePlatformSpecificImplementation<
  //               AndroidFlutterLocalNotificationsPlugin>()
  //           ?.requestPermission() ??
  //       false;
  // }

  // void Function(NotificationResponse) didReceiveLocalNotification() {
  //   return (n) {
  //     // secretOtpCode.value = n.payload;
  //   };
  // }

  Future<String?> recaptchaMethodChannel() async {
    return _doasdk.invokeMethod<String>(
        'validationRecaptcha', recaptchaSiteKey);
  }

  Future<void> sendSmsNotification() async {
    var _payload = randomNumber(5);
    // AndroidNotificationDetails androidNotificationDetails =
    //     AndroidNotificationDetails("${_l.first}", 'DOA SDK',
    //         channelDescription: 'Digital Opening Account Verify Code',
    //         importance: Importance.max,
    //         priority: Priority.high,
    //         ticker: 'ticker');
    // NotificationDetails notificationDetails =
    //     NotificationDetails(android: androidNotificationDetails);
    // await flutterLocalNotificationsPlugin.show(
    //     1, 'Digital Opening Account', _payload, notificationDetails,
    //     payload: _payload);
    DIALOG_HELPER_PERMISSION("Masukan kode berikut untuk testing $_payload");
    secretOtpCode.value = _payload;
    isWhatsappOtp.value = false;
  }

  Future sendWhatsappMethodChannel() async {
    var _phoneNumber = "62${readUserData()?.number}";
    var _text = randomNumber(5);
    secretOtpCode.value = _text;
    isWhatsappOtp.value = true;
    return _doasdk.invokeMethod(
        "sentWhatsappVerifyCode", "$_phoneNumber,$_text");
  }

  void Function()? placeSdk() {
    return () async {
      // var _res = await _doasdk.invokeMethod("place");
    };
  }

  void startTimer(int seconds) {
    const duration = Duration(seconds: 1);
    var remainingSeconds = seconds;
    _timer = Timer.periodic(duration, (Timer timer) {
      if (remainingSeconds < 0) {
        time.value = "0";
        timer.cancel();
      } else {
        int minutes = remainingSeconds ~/ 60;
        int seconds = (remainingSeconds % 60);
        time.value =
            "${minutes.toString().padLeft(2, "0")}:${seconds.toString().padLeft(2, "0")}";
        remainingSeconds--;
      }
    });
  }
}
