// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'dart:async';
import 'package:doasdk/controller/main_controller.dart';
import 'package:flutter/scheduler.dart';
import 'package:doasdk/doasdk.dart';

class InputOtpCodeController extends GetxController {
  final MainController _mController = Get.find();
  List<TextEditingController> inputOtpController =
      List.generate(5, (i) => TextEditingController());
  List<FocusNode> focusNode = List.generate(5, (i) => FocusNode());
  RxString otpTextController = RxString('');
  bool get _isBusyResendOtp => _mController.time.value != "0";
  void Function(RawKeyEvent)? onKey(int i) {
    return (r) {
      if (r.logicalKey.keyLabel != "Backspace") {
        SchedulerBinding.instance.scheduleFrameCallback((_) {
          focusNode[i].nextFocus();
        });
      } else if (i == 0) {
        SchedulerBinding.instance.scheduleFrameCallback((_) {
          focusNode[i].unfocus();
        });
      } else {
        SchedulerBinding.instance.scheduleFrameCallback((_) {
          focusNode[i].previousFocus();
        });
      }
    };
  }

  void Function()? resendOtp() {
    return () async {
      if (_isBusyResendOtp) {
        return;
      } else {
        if (_mController.isWhatsappOtp.value) {
          await _resendWhatsappOtp();
        } else {
          await _resendSmsOtp();
        }
      }
    };
  }

  Future<void> _resendSmsOtp() async {
    if (readUserData()?.number == null) {
      return;
    }
    await Future.delayed(const Duration(seconds: 1));
    await _mController.sendSmsNotification();
    _mController.startTimer(180);
  }

  Future<void> _resendWhatsappOtp() async {
    if (readUserData()?.number == null) {
      return;
    }
    await _mController.sendWhatsappMethodChannel();
    _mController.startTimer(180);
  }

  void Function(String)? seletedNum() {
    return (n) {
      otpTextController.value = n;
    };
  }

  void Function()? next() {
    return () async {
      if (otpTextController.value.length != 5) {
        DIALOG_HELPER("Pastikan kode otp terisi dengan benar");
        return;
      }
      if (otpTextController.value == _mController.secretOtpCode.value) {
        await Get.toNamed(ROUTE.createPinAccount.name);
      } else {
        DIALOG_HELPER("Maaf Kode yg Anda masukan salah");
      }
    };
  }

  @override
  void onReady() {
    _mController.startProgressAnim();
    _mController.startTimer(180);
    super.onReady();
  }
}
