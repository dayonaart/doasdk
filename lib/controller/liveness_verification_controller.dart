// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
// ignore_for_file: no_leading_underscores_for_local_identifiers, unused_local_variable
import 'package:doasdk/controller/main_controller.dart';
import 'package:zoloz_sdk/zoloz_sdk.dart';
import 'package:doasdk/doasdk.dart';

class LivenessVerificationController extends GetxController {
  final MainController _mController = Get.find();
  final _zolozSdk = ZolozSdk();
  bool _isBusy = false;
  // Future<String> copyUIConfigFile() async {
  //   var file = await rootBundle.load("assets/UIConfig.zip");
  //   Directory appDocDir = await getApplicationDocumentsDirectory();
  //   String configFilePath = "${appDocDir.path}/UIConfig.zip";
  //   final buffer = file.buffer;
  //   await File(configFilePath).writeAsBytes(
  //       buffer.asUint8List(file.offsetInBytes, file.lengthInBytes));
  //   return configFilePath;
  // }

  void Function()? next() {
    return () async {
      if (_isBusy) {
        print("Starting verification Please wait...");
        return;
      }
      _isBusy = true;
      var _res = await _zolozSdk.startZoloz(
          initServer: zolozInitServer, checkServer: zolozCheckServer);
      if (_res?.result?.resultCode == "SUCCESS") {
        await _mController.initCameraController(front: true);
        await Get.toNamed(ROUTE.selfieAndKtp.name);
      } else {
        _isBusy = false;
        return;
      }
      _isBusy = false;
    };
  }

  @override
  void onReady() {
    _mController.startProgressAnim();
    try {
      _mController.camController?.dispose();
    } catch (e) {
      return;
    }
    super.onReady();
  }
}
