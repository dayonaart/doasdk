// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:doasdk/controller/main_controller.dart';
import 'package:doasdk/doasdk.dart';

class TakeKtpController extends GetxController {
  final MainController _mController = Get.find();
  String scannedText = "";
  double boxHeight = 248;
  double boxWidth = 365;
  double previewAspectRatio = 0.5;
  Rx<Size> cameraPreviewSize = Rx(Size.zero);
  void Function(Size) onMeasureCameraPreviewSize() {
    return (size) {
      cameraPreviewSize.value = size;
    };
  }

  List<TextSpan> get cameraHelperDescriptionWidget {
    return TakeKtpWord.pastikanPosisi.text.split("").map((e) {
      if (RegExp(r'[&]', caseSensitive: true).hasMatch(e)) {
        return TextSpan(
            text: TakeKtpWord.ktpAsli.text,
            style: textStyleW600(fontSize: 12, fontColor: BLUE_TEXT));
      } else {
        return TextSpan(
            text: e, style: textStyleW500(fontSize: 12, fontColor: BLUE_TEXT));
      }
    }).toList();
  }

  void Function()? onAccept() {
    return () async {
      Get.close(3);
      await Get.toNamed(ROUTE.registrationForm.name);
    };
  }

  void Function()? onReject() {
    return () {
      Get.close(2);
    };
  }

  void Function()? onCompleteCamera() {
    return () async {
      await Get.toNamed(ROUTE.previewTakeImage.name);
    };
  }

  @override
  void onReady() {
    _mController.startProgressAnim();
    super.onReady();
  }
}
