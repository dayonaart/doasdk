// Copyright (c) 2023 Created By Dayona All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package id.doa.sdk

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.Manifest.permission.CAMERA
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager.*
import android.net.Uri
import android.util.Log
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat.*
import androidx.core.content.ContextCompat
// import com.google.android.libraries.places.api.Places
// import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest
// import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse
// import com.google.android.libraries.places.api.net.PlacesClient

import com.google.android.recaptcha.Recaptcha
import com.google.android.recaptcha.RecaptchaAction
import com.google.android.recaptcha.RecaptchaClient
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/** doasdkPlugin */
class DoaSdkPlugin : FlutterPlugin, MethodCallHandler, ActivityAware {
    private lateinit var channel: MethodChannel
    private lateinit var context: Context
    private lateinit var activity: Activity
    private lateinit var siteKey: String
    private lateinit var recaptchaClient: RecaptchaClient
    // private lateinit var placesClient: PlacesClient
    private val permissionRequestCode = 200
    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "doasdk")
        channel.setMethodCallHandler(this)
        context = flutterPluginBinding.applicationContext


    }

    @OptIn(DelicateCoroutinesApi::class)
    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        when (call.method) {
            "checkPermission" -> {
                if (checkPermission()) {
                    Log.d("tag", "CHEKCKING")
                    result.success("GRANTED")
                } else {
                    result.success("REQUESTING")
                    requestPermission()
                }
            }
            "validationRecaptcha" -> {
                siteKey = call.arguments as String
                GlobalScope.launch {
                    initializeRecaptchaClient(result)
                }
            }
            "sentWhatsappVerifyCode" -> {
                val data = call.arguments as String
                sentWhatsappVerifyCode(data)
            }
            "place" -> {
//                findPlace(result)
            }
            else -> {
                result.notImplemented()
            }
        }
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        activity = binding.activity
    }

    override fun onDetachedFromActivityForConfigChanges() {
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
    }

    override fun onDetachedFromActivity() {
    }


    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(
            activity,
            ACCESS_FINE_LOCATION
        )
        val result1 = ContextCompat.checkSelfPermission(
            activity,
            CAMERA
        )
        return result == PERMISSION_GRANTED && result1 == PERMISSION_GRANTED
    }

    private fun requestPermission() {
        requestPermissions(
            activity,
            arrayOf(ACCESS_FINE_LOCATION, CAMERA),
            permissionRequestCode
        )
    }

    @OptIn(DelicateCoroutinesApi::class)
    private suspend fun initializeRecaptchaClient(result: Result) {
        GlobalScope.launch {
            Recaptcha.getClient(activity.application, siteKey)
                .onSuccess { client ->
                    recaptchaClient = client
                    validatedRecaptcha(result)
                }
                .onFailure { exception ->
                    result.success(exception.localizedMessage)
                }
        }
    }

    @OptIn(DelicateCoroutinesApi::class)
    private fun validatedRecaptcha(result: Result) {
        GlobalScope.launch {
            recaptchaClient
                .execute(RecaptchaAction.LOGIN)
                .onSuccess {
                    result.success("Success")

                }
                .onFailure { exception ->
                    result.success(exception.localizedMessage)

                }
        }
    }

    private fun sentWhatsappVerifyCode(data: String) {
        val phoneNumber = data.split(",").first()
        val text = data.split(",").last()
        val browserIntent =
            Intent(Intent.ACTION_VIEW, Uri.parse("https://wa.me/${phoneNumber}?text=$text"))
        browserIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(browserIntent)
    }
//
//    private fun findPlace(@NonNull result: Result
//    ){
//        if (!Places.isInitialized()) {
//            Places.initialize(activity, "AIzaSyDcp_L6xl-f9yNmgEqv15rhERs_cWJGZTQ")
//        }
//        if (Places.isInitialized()){
//            placesClient=Places.createClient(activity)
//          try {
//              val requestBuilder = FindAutocompletePredictionsRequest.builder()
//                .setQuery("")
//                  .setCountries("AU", "NZ")
////                .setOrigin(origin)
////                .setLocationBias(locationBias)
////                .setLocationRestriction(locationRestriction)
////                .setTypesFilter(getTypesFilter())
//              val task = placesClient.findAutocompletePredictions(requestBuilder.build())
//              task.addOnSuccessListener { response: FindAutocompletePredictionsResponse? ->
//                  response?.let {
//                      result.success(it)
//                  }
//              }
//              task.addOnFailureListener { exception: Exception ->
//                  exception.printStackTrace()
//                  result.success(exception.localizedMessage)
//
//              }
//              task.addOnCompleteListener {
//
//              }
//          }catch (e:Exception){
//          result.success(e)
//          }
//        }else{
//            result.success("ERROR INIT PLACE")
//        }
//
//    }
}